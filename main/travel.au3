#include <GUIConstants.au3>
#include <GUIConstantsEx.au3>
#include <ColorConstants.au3>
#include <lib/GWA2.au3>
#include <lib/GwConstants.au3>
#include <myguifuncs.au3>


Func fastTravelBuildUI()
	Local Const $travelX = 10
	Local Const $travelY = 10
	Local $x = $travelX
	Local $y = $travelY
	Local $buttonWidth = 80
	Global Const $travelDistrict = 	GUICtrlCreateCombo("Current District", $travelX, 	$travelY, 175, 25, $CBS_DROPDOWNLIST)
	GUICtrlSetData($travelDistrict, "International|American|American District 1|Europe English|Europe French|Europe German|Europe Italian|Europe Spanish|Europe Polish|Europe Russian|Asian Korean|Asia Chinese|Asia Japanese")
	GUICtrlSetBkColor($travelDistrict, $COLOR_BLACK)

	$x = $travelX
	$y += 45
	Global Const $travelToA = 		MyGuiCtrlCreateButton("ToA", 		$x+5, 	$y,  $buttonWidth, 25)
	Global Const $travelDoA = 		MyGuiCtrlCreateButton("DoA", 		$x+90,	$y,  $buttonWidth, 25)
	$y += 30
	Global Const $travelKamadan = 	MyGuiCtrlCreateButton("Kamadan", 	$x+5, 	$y,  $buttonWidth, 25)
	Global Const $travelEmbark = 	MyGuiCtrlCreateButton("Embark", 	$x+90,	$y,  $buttonWidth, 25)
	$y += 30
	Global Const $travelVlox = 		MyGuiCtrlCreateButton("Vlox's", 	$x+5, $y,  $buttonWidth, 25)
	Global Const $travelEOTN = 		MyGuiCtrlCreateButton("EoTN", 		$x+90, $y,  $buttonWidth, 25)
	$y += 30
	Global Const $travelUrgoz = 	MyGuiCtrlCreateButton("Urgoz", 		$x+5, $y, $buttonWidth, 25)
	Global Const $travelDeep = 		MyGuiCtrlCreateButton("Deep", 		$x+90, $y, $buttonWidth, 25)

	GUICtrlSetOnEvent($travelToA, "fastTravel")
	GUICtrlSetOnEvent($travelDoA, "fastTravel")
	GUICtrlSetOnEvent($travelEmbark, "fastTravel")
	GUICtrlSetOnEvent($travelKamadan, "fastTravel")
	GUICtrlSetOnEvent($travelVlox, "fastTravel")
	GUICtrlSetOnEvent($travelEOTN, "fasttravel")
	GUICtrlSetOnEvent($travelUrgoz, "fastTravel")
	GUICtrlSetOnEvent($travelDeep, "fastTravel")

	$y += 45
	Local $lDestnations = ""
	For $i=1 To $MAP_ID[0]
		If IsString($MAP_ID[$i]) And $MAP_ID[$i] <> "" Then
			$lDestnations = $lDestnations & "|"&$MAP_ID[$i]
		EndIf
	Next
	Global Const $travelDestination = GUICtrlCreateCombo("Select Destination", $travelX, $y, 175, 25, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
	GUICtrlSetData(-1, $lDestnations)
	GUICtrlSetBkColor(-1, $COLOR_BLACK)
	$y += 29
	Global Const $travelButton = MyGuiCtrlCreateButton("Travel", 75, $y, 110, 25)
	GUICtrlSetOnEvent($travelButton, "fastTravel")
EndFunc


Func fastTravel()
	Local $lMapId = 0
	Local $lDistrict
	Local $lDistrictNo = 0
	Local $lRegion[12] = 	[-2, 0, 2, 2, 2, 2, 2, 2,  2, 1, 3, 4]
	Local $lLanguage[12] = 	[0,  0, 0, 2, 3, 4, 5, 9, 10, 0, 0, 0]
	Local $lGuiDistrict = GUICtrlRead($travelDistrict)
	Switch @GUI_CtrlId
		Case $travelDoA
			$lMapId = $MAP_ID_DOA
		Case $travelEmbark
			$lMapId = $MAP_ID_EMBARK
		Case $travelKamadan
			$lMapId = $MAP_ID_KAMADAN
		Case $travelToA
			$lMapId = $MAP_ID_TOA
		Case $travelVlox
			$lMapId = $MAP_ID_VLOX
		Case $travelEOTN
			$lMapId = $MAP_ID_EOTN
		Case $travelUrgoz
			$lMapId = $MAP_ID_URGOZ
		Case $travelDeep
			$lMapId = $MAP_ID_DEEP
		Case $travelButton
			Local $lDestination = GUICtrlRead($travelDestination)
			For $i=1 To $MAP_ID[0]
				If $MAP_ID[$i] == $lDestination Then
					$lMapId = $i
					ExitLoop
				EndIf
			Next
	EndSwitch
	If $lGuiDistrict == "Current District" Then
		If getMapID() == $lMapId Then Return
		MoveMap($lMapId, GetRegion(), 0, GetLanguage())
		Return
	Else
		Switch $lGuiDistrict
			Case "International"
				$lDistrict = 0
			Case "American"
				$lDistrict = 1
			Case "American District 1"
				$lDistrict = 1
				$lDistrictNo = 1
			Case "Europe English"
				$lDistrict = 2
			Case "Europe French"
				$lDistrict = 3
			Case "Europe German"
				$lDistrict = 4
			Case "Europe Italian"
				$lDistrict = 5
			Case "Europe Spanish"
				$lDistrict = 6
			Case "Europe Polish"
				$lDistrict = 7
			Case "Europe Russian"
				$lDistrict = 8
			Case "Asian Korean"
				$lDistrict = 9
			Case "Asia Chinese"
				$lDistrict = 10
			Case "Asia Japanese"
				$lDistrict = 11
		EndSwitch
		If (getMapID()==$lMapId) And (getRegion()==$lRegion[$lDistrict]) And (getLanguage()==$lLanguage[$lDistrict]) Then Return
		MoveMap($lMapId, $lRegion[$lDistrict], $lDistrictNo, $lLanguage[$lDistrict])
	EndIf
EndFunc