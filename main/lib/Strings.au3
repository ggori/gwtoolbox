

Global Const $GWToolbox = "GWToolbox"
Global Const $DataFolder = @AppDataDir&"\"&$GWToolbox&"\"
Global Const $iniFileName = $GWToolbox&".ini"
Global Const $iniFullPath = $DataFolder&$iniFileName
Global Const $licenseFullPath = $DataFolder&"license.lic"
Global Const $keysIniFullPath = $DataFolder&"keys.ini"
Global Const $tmpFullPath = $DataFolder&"tmp"

Global Const $Host = "http://fbgmguild.com/GWToolbox/"

; ini elements
Global Const $s_sfmacro = "sfmacro"
Global Const $s_pcons = "pcons"

; hotkeys
Global Const $s_stuck = "stuck"
Global Const $s_recall = "recall"
Global Const $s_ua = "ua"
Global Const $s_hidegw = "hidegw"
Global Const $s_resign = "resign"
Global Const $s_clicker = "clicker"
Global Const $s_teamresign = "teamresign"
Global Const $s_res = "res"
Global Const $s_age = "age"
Global Const $s_agepm = "agepm"
Global Const $s_pstone = "pstone"
Global Const $s_focus = "focus"
Global Const $s_looter = "looter"
Global Const $s_identifier = "identifier"
Global Const $s_rupt = "rupt"
Global Const $s_movement = "movement"
Global Const $s_ghostpop = "ghostpop"
Global Const $s_ghosttarget = "ghosttarget"
Global Const $s_gstonepop = "gstonepop"
Global Const $s_legiopop = "legiopop"
Global Const $s_rainbowuse = "rainbowpop"


; pcons
Global Const $s_cons = "cons"
Global Const $s_alcohol = "alcohol"
Global Const $s_RRC = "RRC"
Global Const $s_BRC = "BRC"

; ini
Global Const $s_slashkeysini = "\keys.ini"
Global Const $s_keysini = "keys.ini"

Global Const $s_gwtoolboxcurrentversion = "GWToolbox_currentVersion.txt"
Global Const $s_timer = "timer"
Global Const $s_color = "color"
Global Const $s_health = "health"
Global Const $s_party = "party"
Global Const $s_buffs = "buffs"
Global Const $s_distance = "distance"
Global Const $s_Transparency = "Transparency"
