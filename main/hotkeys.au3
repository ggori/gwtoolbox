#include <Misc.au3>

#include <lib/GwConstants.au3>
#include <lib/GwFuncs.au3>
#include <lib/GWA2.au3>
#include <globals.au3>
#include <myguifuncs.au3>
#include <hotkeyUtils.au3>

#cs
this file contains everything related to the hotkeys tab and hotkeys functionality

#ce

Global $hotkeyCount = 23
Global $hotkeyLegitCount = 15
Global Enum $HOTKEY_STUCK, $HOTKEY_RECALL, $HOTKEY_UA, $HOTKEY_RESIGN, $HOTKEY_TEAMRESIGN, _
$HOTKEY_CLICKER, $HOTKEY_RES, $HOTKEY_AGE, $HOTKEY_AGEPM, $HOTKEY_PSTONE, $HOTKEY_GHOSTTARGET, _
$HOTKEY_GHOSTPOP, $HOTKEY_GSTONEPOP, $HOTKEY_LEGIOPOP, $HOTKEY_RAINBOWUSE, _
$HOTKEY_LOOTER, $HOTKEY_IDENTIFIER, $HOTKEY_FOCUS, $HOTKEY_HIDEGW, _		; I dont like them
$HOTKEY_RUPT, $HOTKEY_MOVEMENT, $HOTKEY_DROP1COIN, $HOTKEY_DROPCOINS		; People don't need to know

Local $clickerToggle = False
Local $dropCoinToggle = False
Local $dropCoinTimer = TimerInit()

Global $hotkeyCheckbox[$hotkeyCount]
Global $hotkeyInput[$hotkeyCount]

Local $ruptSkill, $ruptSlot, $ruptActive = False
Local $movementXcoord, $movementYcoord


Global $hotkeyName[$hotkeyCount]
$hotkeyName[$HOTKEY_STUCK] = "stuck"
$hotkeyName[$HOTKEY_RECALL] = "recall"
$hotkeyName[$HOTKEY_UA] = "ua"
$hotkeyName[$HOTKEY_RESIGN] = "resign"
$hotkeyName[$HOTKEY_TEAMRESIGN] = "teamresign"
$hotkeyName[$HOTKEY_CLICKER] = "clicker"
$hotkeyName[$HOTKEY_RES] = "res"
$hotkeyName[$HOTKEY_AGE] = "age"
$hotkeyName[$HOTKEY_AGEPM] = "agepm"
$hotkeyName[$HOTKEY_PSTONE] = "pstone"
$hotkeyName[$HOTKEY_GHOSTTARGET] = "ghosttarget"
$hotkeyName[$HOTKEY_GHOSTPOP] = "ghostpop"
$hotkeyName[$HOTKEY_GSTONEPOP] = "gstonepop"
$hotkeyName[$HOTKEY_LEGIOPOP] = "legiopop"
$hotkeyName[$HOTKEY_RAINBOWUSE] = "rainbowpop"
; from here will be hidden by default
$hotkeyName[$HOTKEY_LOOTER] = "looter"
$hotkeyName[$HOTKEY_IDENTIFIER] = "identifier"
$hotkeyName[$HOTKEY_FOCUS] = "focus"
$hotkeyName[$HOTKEY_HIDEGW] = "hidegw"
$hotkeyName[$HOTKEY_RUPT] = "rupt"
$hotkeyName[$HOTKEY_MOVEMENT] = "movement"
$hotkeyName[$HOTKEY_DROP1COIN] = "drop1coin"
$hotkeyName[$HOTKEY_DROPCOINS] = "dropcoins"

; the function to call when an hotkey is pressed. the function MUST be named "action" and then the name, e.g. actionstuck, actionrecall, etc
Global $hotkeyAction[$hotkeyCount]
For $i = 0 To $hotkeyCount-1
	$hotkeyAction[$i] = "action" & $hotkeyName[$i]
Next

Local $hotkeyPressed[$hotkeyCount]
For $i = 0 To $hotkeyCount-1
	$hotkeyPressed[$i] = False
Next

Global $hotkeyShow[$hotkeyCount]
For $i = 0 To $hotkeyCount-1
	$hotkeyShow[$i] = ($i < $hotkeyLegitCount)
Next

Global $hotkeyActive[$hotkeyCount]
Global $hotkeyKey[$hotkeyCount]

Func hotkeyGetMaxScroll()
	Local $lRet = - 269
	For $i = 0 To $hotkeyCount-1
		If $hotkeyShow[$i] Then $lRet += 31
	Next
	Return $lRet
EndFunc

Func hotkeyLoadIni()
	For $i = 0 To $hotkeyCount-1
		$hotkeyActive[$i] = (IniRead($iniFullPath, $hotkeyName[$i], "active", False) == "True")
		$hotkeyKey[$i] = IniRead($iniFullPath, $hotkeyName[$i], "hotkey", "00")
	Next

	For $i = $hotkeyLegitCount To $hotkeyCount-1
		IniReadSection($iniFullPath, $hotkeyName[$i])
		If Not @error Then $hotkeyShow[$i] = True
	Next

	$ruptSkill = IniRead($iniFullPath, $hotkeyName[$HOTKEY_RUPT], "skill", 0)
	$ruptSlot = IniRead($iniFullPath, $hotkeyName[$HOTKEY_RUPT], "slot", 0)

	$movementXcoord = IniRead($iniFullPath, $hotkeyName[$HOTKEY_MOVEMENT], "x", 0)
	$movementYcoord = IniRead($iniFullPath, $hotkeyName[$HOTKEY_MOVEMENT], "y", 0)
EndFunc

Func hotkeyMakeGroup($Title, $index, $tip)
	If Not $hotkeyShow[$index] Then Return

	Static Local $y = 5
	Local Const $x = 10

	$hotkeyCheckbox[$index] = GUICtrlCreateCheckbox("", $x, $y + 2, 17, 17)
	GUICtrlSetTip(-1, "Enables the hotkey")
	GUICtrlSetOnEvent($hotkeyCheckbox[$index], "hotkeyToggleActive")
	If $hotkeyActive[$index] Then GUICtrlSetState($hotkeyCheckbox[$index], $GUI_CHECKED)

	GUICtrlCreateLabel($Title, $x + 20, $y+3, 80, 20)
	GUICtrlSetFont(-1, 9.5)
	GUICtrlSetTip(-1, $tip)

	$hotkeyInput[$index] = MyGuiCtrlCreateButton("", $x + 110, $y, 64, 20)
	GUICtrlSetTip(-1, "Click to select an hotkey")
	GUICtrlSetOnEvent($hotkeyInput[$index], "setHotkey")
	GUICtrlSetData($hotkeyInput[$index], IniRead($keysIniFullPath, "idToKey", $hotkeyKey[$index], ""))

	GuiCtrlCreateRect($x + 10, $y + 25, 155, 1)

	$y += 31
EndFunc

Func hotkeyBuildUI()
GUICtrlCreateTabItem("Hotkeys")

	hotkeyMakeGroup("/stuck", $HOTKEY_STUCK, _
		"It will send /stuck to Guild Wars. Will also pm you /stuck in order to see that it really was used.")

	hotkeyMakeGroup("Recall", $HOTKEY_RECALL, _
		"If Recall is currently active on self, it will be cancelled, otherwise it will be cast on the current target.")

	hotkeyMakeGroup("UA", $HOTKEY_UA, _
		"If UA is currently active on self it will be cancelled, otherwise it will be cast.")

	hotkeyMakeGroup("/resign", $HOTKEY_RESIGN, _
		"It will send /resign to Guild Wars. Will also pm you /resign in order to see that it really was used.")

	hotkeyMakeGroup("[/resign;xx]", $HOTKEY_TEAMRESIGN, _
		"It will send [/resign;xx] to party chat")

	hotkeyMakeGroup("Res Scrolls", $HOTKEY_RES, _
		"It will use a res scroll from your inventory")

	hotkeyMakeGroup("/age", $HOTKEY_AGE, _
		"It will send /age to Guild Wars")

	hotkeyMakeGroup("Age pm", $HOTKEY_AGEPM, _
		"It will display an in-game message with the current time."&@CRLF& _
		"In Urgoz it will also display the status of the doors")

	hotkeyMakeGroup("Toggle Clicker", $HOTKEY_CLICKER, _
		"Pressing the hotkey will toggle the Clicker."&@CRLF& _
		"When active, it will click very fast")

	hotkeyMakeGroup("Use Pstone", $HOTKEY_PSTONE, _
		"It will use a powerstone.")

	hotkeyMakeGroup("Use Rainbow", $HOTKEY_RAINBOWUSE, _
		"It will use a set of rainbow rocks (red, blue and green)"&@CRLF& _
		"It will not use a rock if it's already active")

	hotkeyMakeGroup("Target Boo", $HOTKEY_GHOSTTARGET, _
		"It will target the closest Boo, the ghost created by ghost-in-a-box"&@CRLF& _
		"When it's targeted you can normally use any skill that targets an ally")

	hotkeyMakeGroup("Pop Ghost", $HOTKEY_GHOSTPOP, _
		"It will use a ghost-in-the-box from your inventory")

	hotkeyMakeGroup("Pop Ghastly", $HOTKEY_GSTONEPOP, _
		"It will use a Ghastly Summoning Stone")

	hotkeyMakeGroup("Pop Legion.", $HOTKEY_LEGIOPOP, _
		"It will use a Legionnaire Summoning Crystal")

	hotkeyMakeGroup("Gate Macro", $HOTKEY_MOVEMENT, _
		"It will move your character to a set position"&@CRLF& _
		"You have to manually set the x and y coordinates in the config file")

	hotkeyMakeGroup("Rupt", $HOTKEY_RUPT, _
		"It will use the selected skill if the target if using the other selected skill" & @CRLF & _
		"You have to manually set the slot and skill ID in the config file")

	hotkeyMakeGroup("Drop Coins", $HOTKEY_DROPCOINS, _
		"It will drop a gold coin every 400 ms")

	hotkeyMakeGroup("Drop 1 Coin", $HOTKEY_DROP1COIN, _
		"It will drop one gold coin")

	hotkeyMakeGroup("Looter", $HOTKEY_LOOTER, _
		"It will pickup all assigned items in range 'nearby' of the player")

	hotkeyMakeGroup("Identifier", $HOTKEY_IDENTIFIER, _
		"It will identify all items in inventory")

	hotkeyMakeGroup("Hide GW", $HOTKEY_HIDEGW, _
		"It will hide the Guild Wars window and Toolbox,"&@CRLF& _
		"You will be able to click on the gwtoolbox icon in the tray area to restore")

	hotkeyMakeGroup("Focus", $HOTKEY_FOCUS, _
		"By pressing this hotkey the toolbox will swap the focus between GW and the Toolbox itself."&@CRLF& _
		"Very useful when playing with toolbox non always on top."&@CRLF& _
		"If neither toolbox nor Gw are on top, GW will be given focus")
EndFunc

Func hotkeyMainLoop($hDLL)
	Local $pressed

	For $i = 0 To $hotkeyCount-1
		If $hotkeyActive[$i] Then
			$pressed = _IsPressed($hotkeyKey[$i], $hDLL)
			If (Not $hotkeyPressed[$i]) And $pressed Then
				$hotkeyPressed[$i] = True
				Call($hotkeyAction[$i])
			ElseIf $hotkeyPressed[$i] And (Not $pressed) Then
				$hotkeyPressed[$i] = False
			EndIf
		EndIf
	Next

	If $ruptActive And GetMapLoading()==$INSTANCETYPE_EXPLORABLE And (Not GetIsDead(-2)) Then
		Local $lTgt = GetAgentByID(-1)
		Local $skill = DllStructGetData($lTgt, "Skill")
		If $skill == $ruptSkill And (DllStructGetData(GetSkillBar(), "Recharge" & $ruptSlot)==0) Then
			UseSkill($ruptSlot, -1)
			pingSleep()
		EndIf
	EndIf

	If $clickerToggle Then
		For $i=1 To 10
			MouseClick("left")
		Next
	EndIf

	If $dropCoinToggle And GetMapLoading()==$INSTANCETYPE_EXPLORABLE And (Not GetIsDead(-2)) Then
		If TimerDiff($dropCoinTimer) > 400 Then
			DropGold(1)
			$dropCoinTimer = TimerInit()
		EndIf
	EndIf
EndFunc

Func isGwOnTop()
	Return WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)
EndFunc

Func actionstuck()
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	If Not isGwOnTop() Then Return

	SendChat("stuck", "/")
	WriteChat("/stuck", $GWToolbox)
EndFunc

Func actionrecall()
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not isGwOnTop() Then Return

	Local $hasRecall = GetIsTargetBuffed($SKILL_ID_RECALL, -2)
	If $hasRecall Then
		DropBuff($SKILL_ID_RECALL, -2)
	Else
		Local $skillBar = getSkillBar()
		Local $skillNo = getSkillPosition($SKILL_ID_RECALL, $skillBar)
		If $skillNo > 0 And DllStructGetData($skillBar, "Recharge" & $skillNo) == 0 Then
			useSkill($skillNo, -1)
		EndIf
	EndIf
EndFunc

Func actionua()
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not isGwOnTop() Then Return

	Local $hasUA = GetIsTargetBuffed($SKILL_ID_UA, -2)
	If $hasUA Then
		DropBuff($SKILL_ID_UA, -2)
	Else
		Local $skillBar = getSkillBar()
		Local $skillNo = getSkillPosition($SKILL_ID_UA, $skillBar)
		If $skillNo > 0 And DllStructGetData($skillBar, "Recharge" & $skillNo) == 0 Then
			useSkill($skillNo, -1)
		EndIf
	EndIf
EndFunc

Func actionhidegw()
	WinSetState($gwHWND, "", @SW_HIDE)
	GUISetState(@SW_HIDE, $mainGui)
	GUISetState(@SW_HIDE, $dummyGui)
	Opt("TrayIconHide", False)
	Opt("GUIOnEventMode", False)

	Local $restore = TrayCreateItem("Restore")
	TrayCreateItem("")
	Local $close = TrayCreateItem("Close toolbox and guild wars")

	TraySetState()

	Local $hDLL = DllOpen("user32.dll")

	While 1
		Local $msg = TrayGetMsg()
		Local $pressed = _IsPressed($hotkeyKey[$HOTKEY_HIDEGW], $hDLL)
		If ($hotkeyActive[$HOTKEY_HIDEGW] And $pressed) Or $msg = $restore Then
			If $pressed Then
				While _IsPressed($hotkeyKey[$HOTKEY_HIDEGW], $hDLL)
					Sleep(10)
				WEnd
			EndIf

			WinSetState($gwHWND, "", @SW_SHOW)
			GUISetState(@SW_SHOW, $mainGui)
			GUISetState(@SW_SHOW, $dummyGui)
			Opt("TrayIconHide", True)
			Opt("GUIOnEventMode", True)
			Return

		ElseIf $msg = $close Then
			WinClose($gwHWND)
			Exit
		EndIf
	WEnd
EndFunc   ;==>sendToggleHide

Func actionclicker()
	$clickerToggle = Not $clickerToggle
	If GetMapLoading()<>$INSTANCETYPE_LOADING Then WriteChat("Clicker: " & ($clickerToggle ? "On" : "Off"), $GWToolbox)
EndFunc

Func actiondrop1coin()
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not isGwOnTop() Then Return
	If GetIsDead(-2) Then Return
	DropGold(1)
EndFunc

Func actiondropcoins()
	If Not isGwOnTop() Then Return
	$dropCoinToggle = Not $dropCoinToggle
	If GetMapLoading()<>$INSTANCETYPE_LOADING Then WriteChat("Coin dropper: " & ($dropCoinToggle ? "On" : "Off"), $GWToolbox)
EndFunc

Func actionresign()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	SendChat("resign", "/")
	WriteChat("/resign", $GWToolbox)
EndFunc

Func actionteamresign()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	SendChat("[/resign;xx]", "#")
EndFunc

Func actionres()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not UseItemByModelID($ITEM_ID_RES_SCROLLS) Then WriteChat("[WARNING] Res scroll not found!", $GWToolbox)
EndFunc

Func actionage()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	SendChat("age", "/")
EndFunc

Func actionagepm()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return

	Local $lUptime = GetInstanceUptime()
	Local $sec = Int($lUptime/1000)
	Local $min = Int($sec/60)
	Local $msg = Int($min/60)&":"&StringFormat("%02d", Mod($min, 60))&":"&StringFormat("%02d", Mod($sec, 60))
	If GetMapID() == $MAP_ID_URGOZ And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
		Local $temp = Mod($sec, 25)
		If $temp < 1 Then
			$msg &= " - Doors CLOSED" & " - opening in 0 sec(s)"
		ElseIf $temp < 16 Then
			$msg &= " - Doors OPEN" & " - closing in "&(15-$temp)&" sec(s)"
		Else
			$msg &= " - Doors CLOSED" & " - opening in "&(25-$temp)&" sec(s)"
		EndIf
	EndIf
	WriteChat($msg, $GWToolbox)
EndFunc

Func actionpstone()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not UseItemByModelID($ITEM_ID_POWERSTONE) Then WriteChat("[WARNING] Powerstone not found!", $GWToolbox)
EndFunc

Func actionfocus()
	If WinActive($gwHWND) Then
		WinActivate($mainGui)
	Else
		WinActivate($gwHWND)
	EndIf
EndFunc

Func actionlooter()
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not isGwOnTop() Then Return

	Local $lMe = GetAgentByID(-2)
	Local $lItemArray[1] = [0]
	Local $lAgentArray = GetAgentArray(0x400)

	For $i=1 To $lAgentArray[0]
		If DllStructGetData($lAgentArray[$i], "Owner")==0 Or DllStructGetData($lAgentArray[$i], "Owner")==GetMyID() Then
			If GetDistance($lAgentArray[$i], $lMe) < $RANGE_AREA Then
				$lItemArray[0] += 1
				ReDim $lItemArray[$lItemArray[0]+1]
				$lItemArray[$lItemArray[0]] = $lAgentArray[$i]
			EndIf
		EndIf
	Next

	Local $lPseudoDistance, $lOtherPseudoDistance
	Local $lClosestItemIndex
	Local $lClosestItem
	Local $lItemID
	Local $lDeadlock
	While ($lItemArray[0] > 0)

;~ 		1. Choose the closest item
		$lMe = GetAgentByID(-2)
		$lPseudoDistance = $RANGE_AREA ^ 2
		For $i=1 To $lItemArray[0]
			$lOtherPseudoDistance = GetPseudoDistance($lItemArray[$i], $lMe)
			If $lOtherPseudoDistance < $lPseudoDistance Then
				$lPseudoDistance = $lOtherPseudoDistance
				$lClosestItemIndex = $i
			EndIf
		Next

;~ 		2. remove item from list
		$lClosestItem = __ArrayDelete($lItemArray, $lClosestItemIndex)

;~ 		2. Pick up the item
		$lItemID = DllStructGetData($lClosestItem, "ID")
		If GetAgentExists($lItemID) Then SendPacket(0xC, 0x38, $lItemID, 0)

;~ 		3. Wait until the item has been picked up.
		$lDeadlock = TimerInit()
		While GetAgentExists($lItemID)
			Sleep(50)
			If TimerDiff($lDeadlock) > 2500 Then Return
		WEnd
	WEnd
EndFunc

Func actionidentifier()
	Local $lBag, $lItem

	For $lBagNo = 1 To 4
		$lBag = GetBag($lBagNo)
		For $i = 1 To DllStructGetData($lBag, 'Slots')
			$lItem = GetItemBySlot($lBagNo, $i)
			If DllStructGetData($lItem, 'ModelID') == 0 Then ContinueLoop
			If DllStructGetData($lItem, 'ID') == 0 Then ContinueLoop

			If FindIDKit() == 0 Then
				WriteChat("Could not find an Identification Kit", $GWToolbox)
				Return
			EndIf

			IdentifyItem($lItem)
			Sleep(GetPing())
		Next
		Sleep(50)
	Next
EndFunc

Func actionghostpop()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not UseItemByModelID($MODELID_GHOST_IN_THE_BOX) Then WriteChat("[WARNING] Ghost-in-the-Box not found!", $GWToolbox)
EndFunc

Func actionghosttarget()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return

	Local $lMe = GetAgentByID(-2)
	Local $lDistance = 10000000
	Local $lClosest = -1
	Local $lArr = GetAgentArray(0xDB)
	Local $lTmp
	For $i=1 To $lArr[0]
		If DllStructGetData($lArr[$i], 'PlayerNumber') == $MODELID_BOO And DllStructGetData($lArr[$i], 'HP') > 0 Then
			$lTmp = GetPseudoDistance($lMe, $lArr[$i])
			If $lTmp < $lDistance Then
				$lClosest = $i
				$lDistance = $lTmp
			EndIf
		EndIf
	Next
	If $lClosest > 0 Then ChangeTarget($lArr[$lClosest])
EndFunc

Func actiongstonepop()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not UseItemByModelID($MODELID_GSTONE) Then WriteChat("[WARNING] Ghastly Summoning Stone not found!", $GWToolbox)
EndFunc

Func actionlegiopop()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return
	If Not UseItemByModelID($MODELID_LEGIONNAIRE_STONE) Then WriteChat("[WARNING] Legionnaire Summoning Crystal not found!", $GWToolbox)
EndFunc

Func actionrainbowpop()
	If Not isGwOnTop() Then Return
	If GetMapLoading() <> $INSTANCETYPE_EXPLORABLE Then Return

	Local $effectStructRed = GetEffect($EFFECT_REDROCK)
	If DllStructGetData($effectStructRed, "SkillID") == 0 Then
		If Not UseItemByModelID($ITEM_ID_RRC) Then
			WriteChat("[WARNING] Red Rock Candy not found!", $GWToolbox)
		EndIf
	EndIf

	Local $effectStructBlue = GetEffect($EFFECT_BLUEROCK)
	If DllStructGetData($effectStructBlue, "SkillID") == 0 Then
		If Not UseItemByModelID($ITEM_ID_BRC) Then
			WriteChat("[WARNING] Blue Rock Candy not found!", $GWToolbox)
		EndIf
	EndIf

	Local $effectStructGreen = GetEffect($EFFECT_GREENROCK)
	If DllStructGetData($effectStructGreen, "SkillID") == 0 Then
		If Not UseItemByModelID($ITEM_ID_GRC) Then
			WriteChat("[WARNING] Green Rock Candy not found!", $GWToolbox)
		EndIf
	EndIf
EndFunc

Func actionrupt()
	If Not isGwOnTop() Then Return
	$ruptActive = Not $ruptActive
	If GetMapLoading()<>$INSTANCETYPE_LOADING Then WriteChat("Rupt macro: " & ($ruptActive ? "On" : "Off"), $GWToolbox)
EndFunc

Func actionmovement()
	If Not isGwOnTop() Then Return
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	If $movementXcoord == 0 And $movementYcoord == 0 Then Return

	Move($movementXcoord, $movementYcoord, 5)
	WriteChat("Movement macro activated", $GWToolbox)
EndFunc


Func __ArrayDelete(ByRef $aArray, $iElement)
	If $iElement < 1 Then Return
	If $iElement > $aArray[0] Then Return
	Local $ret = $aArray[$iElement]
	For $i = $iElement To $aArray[0]-1
		$aArray[$i] = $aArray[$i+1]
	Next
	ReDim $aArray[$aArray[0]]
	$aArray[0] -= 1
	Return $ret
EndFunc


Func hotkeyToggleActive()
	For $i = 0 To $hotkeyCount-1
		If @GUI_CtrlId == $hotkeyCheckbox[$i] Then
			Local $active = (GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED)
			$hotkeyActive[$i] = $active
			IniWrite($iniFullPath, $hotkeyName[$i], "active", $active)
			Return
		EndIf
	Next
	MyGuiMsgBox(0, "hotkeyToggleActive", "not implemented!")
EndFunc




