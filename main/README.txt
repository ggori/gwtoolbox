GWToolbox Readme File
---------------------

by Has


### Requirements
Autoit 3.3.12 or later is required to run and compile GWToolbox.

SciTE is required to compile the application and it is the recommended program to edit the source code.

both can be downloaded here: https://www.autoitscript.com/site/autoit/downloads/


### How to run
There are various way to run the application:
- Double click the script in explorer (if autoit is set to run scripts)
- Open the script in SciTE and run (F5)
- Compile the script and run the executable


### How to compile
In order to compile GWToolbox you need to open it in SciTE and press build (F7).

right clicking the script and pressing compile will NOT WORK. This is because the compilation process also adds some external files, and those directives are ignored if you compile it outside of SciTE.


### Edit the source code
First of all let me say that the source code is horrible. GWToolbox started as a small script, and then I added more and more features, that's why the code is so long, messy and fully of code duplication. Sorry for that, I should have rewritten it from scratch ages ago but I always been too lazy.

Feel free to edit the source code, there are just a few things you may want to pay attention to:

- If you edit the source, you probably want to disable automatic updates, since any update will delete your changes. You can easily do so by setting to False the variable $do_update in line 48.

- DO care about the performance issues. GWToolbox has to do so much stuff, and AutoIt is so slow, that I had to put special care into this. The Main loop first of all updates all variables of the "world" (only if needed) which are later used by all other features. The function that takes the longer to execute is GetAgentArray(), and that should only be executed once per loop. A lot of GWA2 function hide it inside, so be careful.

- It is very easy to create subtle timing bugs. I had many regarding alcohol usage, so pay attention to when you are doing stuff.

- If you create something interesting, you can write me about it, and maybe I can add it to the public version :)


### Copyright
Copyright 2014 Has Kha

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
