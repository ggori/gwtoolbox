#NoTrayIcon
#RequireAdmin

; Images and compiler instructions
#AutoIt3Wrapper_Icon=img/Monster_skill.ico
#AutoIt3Wrapper_Outfile=GWToolbox.exe
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_File_Add=img/balthspirit.jpg, rt_rcdata, BALTHSPIRIT_JPG
#AutoIt3Wrapper_Res_File_Add=img/lifebond.jpg, rt_rcdata, LIFEBOND_JPG
#AutoIt3Wrapper_Res_File_Add=img/protbond.jpg, rt_rcdata, PROTBOND_JPG
#AutoIt3Wrapper_Res_File_Add=img/comment.jpg, rt_rcdata, comment_JPG
#AutoIt3Wrapper_Res_File_Add=img/cupcake.jpg, rt_rcdata, cupcake_JPG
#AutoIt3Wrapper_Res_File_Add=img/feather.jpg, rt_rcdata, feather_JPG
#AutoIt3Wrapper_Res_File_Add=img/keyboard.jpg, rt_rcdata, keyboard_JPG
#AutoIt3Wrapper_Res_File_Add=img/list.jpg, rt_rcdata, list_JPG
#AutoIt3Wrapper_Res_File_Add=img/plane.jpg, rt_rcdata, plane_JPG
#AutoIt3Wrapper_Res_File_Add=img/settings.jpg, rt_rcdata, settings_JPG
#AutoIt3Wrapper_Run_Obfuscator=n
;#Obfuscator_Parameters=/mergeonly

; Include autoit stuff
#include <Constants.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <ComboConstants.au3>
#include <Crypt.au3>
#include <GUITab.au3>
#include <Math.au3>
#include <EditConstants.au3>
#include <GuiComboBox.au3>
#include <Misc.au3>
#include <GuiScrollBars.au3>

; Include external Libraries
#include <lib/ColorPicker.au3>
#include <lib/ResourcesEx.au3>

; Include GWA2 and other GW stuff
#include <lib/GWA2.au3>
#include <lib/GwConstants.au3>
#include <lib/GwFuncs.au3>

; Include constants and other useful files
#include <constants.au3>
#include <globals.au3>
#include <myguifuncs.au3>

; feature files
#include <pcons.au3>
#include <hotkeys.au3>
#include <travel.au3>
#include <settings.au3>
#include <builds.au3>
#include <dialogs.au3>
#include <hotkeyUtils.au3>
#include <materials.au3>

; Set some global options
Opt("GUIOnEventMode", True)
Opt("TrayMenuMode", True)
Opt("MustDeclareVars", True)
Opt("GUICloseOnESC", False)

If Not FileExists($DataFolder) Then DirCreate($DataFolder)
If Not FileExists($keysIniFullPath) Then FileInstall(".\keys.ini", $keysIniFullPath)

; license and disclaimer if first run (ini file is not there)
Global Const $s_disclaimer = "DISCLAIMER:"&@CRLF& _
	'THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF ' & _
	'FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY ' & _
	'DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.' & @CRLF&@CRLF & _
	'By clicking the OK button you agree with all the above.'
If Not FileExists($iniFullPath) Then
	If MyGuiMsgBox(1, $GWToolbox, $s_disclaimer, 0, 400, 230) <> 1 Then exitProgram()
EndIf

; Initialize GW client
MyInitialize()

; Get window handle and PID
$gwHWND = GetWindowHandle()
$gwPID = WinGetProcess($gwHWND)

Global $timerGui, $timerGuiLabel
Global $healthGui, $healthGuiLabelHP, $healthGuiLabelMaxHP, $healthGuiLabelPerc, $healthGuiCurrentColor
Global $distanceGui, $distanceGuiLabelDist, $distanceGuiLabelPerc, $distanceGuiLabelText, $distanceGuiCurrentColor
Global $partyGui, $partyLabels[9]
Global $buffsGui, $buffsImages[9][3], $buffsStatus[9][3], $buffsImagesHidden[9][3], $buffsID[9][3], $buffsX = 0, $buffsY = 0
Global Enum $BUFFS_VISIBLE, $BUFFS_HIDDEN, $BUFFS_UNKNOWN

Global $hDownload
If $do_update Then
	$hDownload = InetGet($Host&"GWToolbox_currentVersion.txt", $tmpFullPath, 1, 1)
Else
	$hDownload = 0
EndIf

;~ Remove default windows theme, so that custom colors can be used in the GUI
DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)

#region READ INI FILE
Global $COLOR_TIMER = IniRead($iniFullPath, "timer", "color", $COLOR_TIMER_DEFAULT)

Global $COLOR_HEALTH_HIGH = IniRead($iniFullPath, "health", "color1", $COLOR_TIMER_DEFAULT)
Global $COLOR_HEALTH_MIDDLE = IniRead($iniFullPath, "health", "color2", $COLOR_YELLOW)
Global $COLOR_HEALTH_LOW = IniRead($iniFullPath, "health", "color3", $COLOR_RED)

Global $COLOR_PARTY = IniRead($iniFullPath, "party", "color", $COLOR_TIMER_DEFAULT)

Global $COLOR_DISTANCE = IniRead($iniFullPath, "distance", "color", $COLOR_TIMER_DEFAULT)

Global $sfmacro = 	(IniRead($iniFullPath, "sfmacro", 		"active", False) == "True")

Global $timer = 	(IniRead($iniFullPath, "timer", 		"active", False) == "True")
Global $health = 	(IniRead($iniFullPath, "health", 		"active", False) == "True")
Global $party = 	(IniRead($iniFullPath, "party", 		"active", False) == "True")
Global $buffs = 	(IniRead($iniFullPath, "buffs", 		"active", False) == "True")
Global $buffsDrop = (IniRead($iniFullPath, "buffsdrop", 	"active", False) == "True")
Global $distance = 	(IniRead($iniFullPath, "distance",	 	"active", False) == "True")

pconsLoadIni()

hotkeyLoadIni()

dialogsLoadIni()

settingsLoadIni()
#endregion Inifile

#region GUI
Local $x, $y ; for GUI construction
Global Const $MainWidth = 100
Global Const $UtilityWidth = 400
Global Const $MenuBarHeight = 21
Global Const $MainHeight = 300
Global Const $TabButtonHeight = 27

$dummyGui = GUICreate("")
Global $x = IniRead($iniFullPath, "display", "x", -1)
Global $y = IniRead($iniFullPath, "display", "y", -1)
If $x <> -1 And $x+$MainWidth > @DesktopWidth Then $x = @DesktopWidth-$MainWidth
If $x <> -1 And $x < 0 Then $x = 0
If $y <> -1 And $y+$MainHeight > @DesktopHeight Then $y = @DesktopHeight-$MainHeight
If $y <> -1 And $y < 0 Then $y = 0
$mainGui = GUICreate($GWToolbox, $MainWidth, $MainHeight, $x, $y, $WS_POPUP)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	GUISetOnEvent($GUI_EVENT_CLOSE, "exitProgram")
	GUIRegisterMsg($WM_MOUSEWHEEL, "mouseWheelHandler")
$x = $MainWidth - 18
GUICtrlCreateLabel("x", $x+1, 0, 17, 20, BitOR($SS_CENTER, $SS_CENTERIMAGE))
	GUICtrlSetOnEvent(-1, "exitProgram")
GuiCtrlCreateRect($x, 0, 1, 20)
$x -= 18
GUICtrlCreateLabel("-", $x+1, 0, 17, 20, BitOR($SS_CENTER, $SS_CENTERIMAGE))
	GUICtrlSetOnEvent(-1, "minimizeProgram")
GuiCtrlCreateRect($x, 0, 1, 20)

Global Const $dragButton = GUICtrlCreateLabel("GWToolbox", 0, 0, $x-1, 20, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
	GUICtrlSetOnEvent(-1, "mainGuiEventHandler")

; horizontal line on top
GuiCtrlCreateRect(0, 20, 390, 1)

; vertical line in middle
GuiCtrlCreateRect(171, 20, 1, 273)


Global Const $tabCount = 7
Global Enum $TAB_PCONS, $TAB_HOTKEYS, $TAB_BUILDS, $TAB_FASTTRAVEL, $TAB_DIALOGS, $TAB_MATERIALS, $TAB_SETTINGS, $TAB_NONE = -1
Global $currentTab = $TAB_NONE
Global $tabButtons[$tabCount]

GUISetFont(10)
$y = $MenuBarHeight
$tabButtons[$TAB_PCONS] = MyGuiCtrlCreateTab("Pcons", $y, "cupcake")
$CheckboxPCons = GUICtrlCreateCheckbox("", 80, $y, 20, $TabButtonHeight-1)
GUICtrlSetOnEvent($CheckboxPCons, "pconsToggleActive")
$y += $TabButtonHeight
$tabButtons[$TAB_HOTKEYS] = MyGuiCtrlCreateTab("Hotkeys", $y, "keyboard")
$y += $TabButtonHeight
$tabButtons[$TAB_BUILDS] = MyGuiCtrlCreateTab("Builds", $y, "list")
$y += $TabButtonHeight
$tabButtons[$TAB_FASTTRAVEL] = MyGuiCtrlCreateTab("Fast Travel", $y, "plane")
$y += $TabButtonHeight
$tabButtons[$TAB_DIALOGS] = MyGuiCtrlCreateTab("Dialogs", $y, "comment")
$y += $TabButtonHeight
$tabButtons[$TAB_MATERIALS] = MyGuiCtrlCreateTab("Materials", $y, "feather")
$y += $TabButtonHeight
$tabButtons[$TAB_SETTINGS] = MyGuiCtrlCreateTab("Settings", $y, "settings")
$y += $TabButtonHeight + 5
For $i = 0 To $tabCount-1
	GUICtrlSetOnEvent($tabButtons[$i], "tabEventHandler")
Next
Global Const $infoTimerCheckbox = GUICtrlCreateCheckbox("Timer", 7, $y, 73, 17)
GUICtrlSetOnEvent(-1, "toggleTimer")
If $timer Then GUICtrlSetState(-1, $GUI_CHECKED)
$y += 20
Global Const $infoHealthChckbox = GUICtrlCreateCheckbox("Health", 7, $y, 73, 17)
GUICtrlSetOnEvent(-1, "toggleHealth")
If $health Then GUICtrlSetState(-1, $GUI_CHECKED)
$y += 20
Global Const $infoDistanceCheckbox = GUICtrlCreateCheckbox("Distance", 7, $y, 73, 17)
GUICtrlSetOnEvent(-1, "toggleDistance")
If $distance Then GUICtrlSetState(-1, $GUI_CHECKED)
$y += 20
Global Const $infoBuffsCheckbox = GUICtrlCreateCheckbox("Bonds", 7, $y, 73, 17)
GUICtrlSetOnEvent(-1, "toggleBuffs")
If $buffs Then GUICtrlSetState(-1, $GUI_CHECKED)
$y += 20

GUISetFont(8.5)

Global Const $UtilityHeight = $MainHeight - $MenuBarHeight

Global $tabLeftPanel[$tabCount]
Global $tabMainPanel[$tabCount]
Global $tabScrollPos[$tabCount]
Global $tabScrollMax[$tabCount]
Global $tabPanelWidth[$tabCount]
$tabPanelWidth[$TAB_PCONS] = 400
$tabPanelWidth[$TAB_HOTKEYS] = 195
$tabPanelWidth[$TAB_BUILDS] = 195
$tabPanelWidth[$TAB_SETTINGS] = 210
$tabPanelWidth[$TAB_FASTTRAVEL] = 195
$tabPanelWidth[$TAB_DIALOGS] = 400
$tabPanelWidth[$TAB_MATERIALS] = 300

$tabScrollMax[$TAB_PCONS] = 0
$tabScrollMax[$TAB_HOTKEYS] = hotkeyGetMaxScroll()
$tabScrollMax[$TAB_BUILDS] = $buildsMaxScroll
$tabScrollMax[$TAB_SETTINGS] = 410
$tabScrollMax[$TAB_FASTTRAVEL] = 0
$tabScrollMax[$TAB_DIALOGS] = 0
$tabScrollMax[$TAB_MATERIALS] = 0

Local $tabY = $MenuBarHeight + 3
Local $tabLeftPanelX, $tabMainPanelX
For $tabIndex = 0 To $tabCount - 1
	If $expandLeft Then
		$tabLeftPanelX = 2
		$tabMainPanelX = 3-$tabPanelWidth[$tabIndex]
	Else
		$tabLeftPanelX = $MainWidth+3
		$tabMainPanelX = $MainWidth+4
	EndIf

	$tabScrollPos[$tabIndex] = 0
	$tabLeftPanel[$tabIndex] = GUICreate("", 1, $UtilityHeight, $tabLeftPanelX, $tabY, $WS_POPUP, $WS_EX_MDICHILD, $mainGui)
	GUISetBkColor($COLOR_WHITE)
	GuiCtrlCreateRect(0, $TabButtonHeight * $tabIndex, 1, $TabButtonHeight-1, $COLOR_BLACK)
	$tabMainPanel[$tabIndex] = GUICreate("", $tabPanelWidth[$tabIndex]-1,  $UtilityHeight, $tabMainPanelX, $tabY, $WS_POPUP, $WS_EX_MDICHILD, $MainGui)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	GUIRegisterMsg($WM_MOUSEWHEEL, "mouseWheelHandler")

	Switch $tabIndex
		Case $TAB_PCONS
			pconsBuildUI()
		Case $TAB_HOTKEYS
			hotkeyBuildUI()
		Case $TAB_BUILDS
			buildsBuildUI()
		Case $TAB_SETTINGS
			settingsBuildUI()
		Case $TAB_FASTTRAVEL
			fastTravelBuildUI()
		Case $TAB_DIALOGS
			dialogsBuildUI()
		Case $TAB_MATERIALS
			materialsBuildUI()
	EndSwitch
Next

GUIRegisterMsg($WM_MOUSEWHEEL, "mouseWheelHandler")

WinSetTrans($MainGui, "", $transparency)
For $i = 0 To $tabCount - 1
	WinSetTrans($tabLeftPanel[$i], "", $transparency)
	WinSetTrans($tabMainPanel[$i], "", $transparency)
Next


If $timer Then
	$timer = Not $timer
	toggleTimer()
EndIf

If $health Then
	$health = Not $health
	toggleHealth()
EndIf

If $party Then
	$party = Not $party
	toggleParty()
EndIf

If $distance Then
	$distance = Not $distance
	toggleDistance()
EndIf

If $buffs Then
	$buffs = Not $buffs
	toggleBuffs()
EndIf

WinSetOnTop($MainGui, "", $isOnTop)
GUISetState(@SW_SHOW, $mainGui)
#endregion GUI

Func mainLoop()
	#region declarations
	Local $currentMap = -1
	Local $currentLoadingState = -1

	; variable for the pressed status
	Local $pressed = False

	; open the DLL used for _isPressed(..)
	Local $hDLL = DllOpen("user32.dll")

	; stuff for info
	Local $lTgtHP, $lTgtMaxHP

	; variable for the timer
	Local $timerDefaultColor = True
	Local $currentTime, $sec, $min

	Local $lAgentArray = GetAgentArray()
	Local $lParty = GetParty($lAgentArray)
	Local $lPartyDanger = GetPartyDanger($lAgentArray, $lParty)
	Local $lMe = GetAgentByID(-2)
	Local $lTgt = GetAgentByID(-1)

	Local $lBuff, $lBuffTarget, $lBuffTargetNumber
	#endregion

	While True
		If Not ProcessExists($gwPID) Then
			exitProgram()
		EndIf

		#region GetStuff
		$lMe = GetAgentByID(-2)
		$lTgt = GetAgentByID(-1)
		If GetMapLoading() == $INSTANCETYPE_EXPLORABLE Then
			If $pcons And (($pconsActive[$PCONS_CONS] And GetInstanceUpTime() < 61*1000) Or $pconsActive[$PCONS_RES]) Or $party Then
				$lAgentArray = GetAgentArray()
				$lParty = GetParty($lAgentArray)
			EndIf
		EndIf
		#endregion

		#region Transparency
		If GUICtrlRead($TransparencySlider) <> $Transparency Then
			$Transparency = GUICtrlRead($TransparencySlider)
			WinSetTrans($mainGui, "", $Transparency)
			For $i = 0 To $tabCount-1
				WinSetTrans($tabLeftPanel[$i], "", $Transparency)
				WinSetTrans($tabMainPanel[$i], "", $Transparency)
			Next
			If $timer Then WinSetTrans($timerGui, "", $Transparency)
			If $health Then WinSetTrans($healthGui, "", $Transparency)
			If $distance Then WinSetTrans($distanceGui, "", $Transparency)
		EndIf
		#endregion

		#region partydanger
		If $party Then
			$lPartyDanger = GetPartyDanger($lAgentArray, $lParty)
			If GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
				For $i=1 To _Min($lPartyDanger[0], 8)
					If DllStructGetData($lParty[$i], "PlayerNumber") <= 8 Then
						GuiCtrlUpdateData($partyLabels[DllStructGetData($lParty[$i], "PlayerNumber")], $lPartyDanger[$i]); &" - "&GetPlayerName($lParty[$i]))
					EndIf
				Next
			Else
				For $i=1 To 8
					GuiCtrlUpdateData($partyLabels[$i], "-")
				Next
			EndIf
		EndIf
		#endregion

		#region buffs
		If $buffs Then
			For $i=1 To 8
				For $k=0 To 2
					$buffsStatus[$i][$k] = $BUFFS_UNKNOWN
				Next
			Next
			For $buffIndex=1 To GetBuffCount()
				$lBuff = GetBuffByIndex($buffIndex)
				$lBuffTarget = GetAgentByID(DllStructGetData($lBuff, "TargetID"))
				$lBuffTargetNumber = DllStructGetData($lBuffTarget, "PlayerNumber")
				If $lBuffTargetNumber > 0 And $lBuffTargetNumber < 9 Then
					Switch DllStructGetData($lBuff, "SkillID")
						Case 242 ; balth
							buffsShow($lBuffTargetNumber, 0)
							$buffsID[$lBuffTargetNumber][0] = DllStructGetData($lBuff, "BuffID")
						Case 241 ; lifebond
							buffsShow($lBuffTargetNumber, 1)
							$buffsID[$lBuffTargetNumber][1] = DllStructGetData($lBuff, "BuffID")
						Case 263 ; prot
							buffsShow($lBuffTargetNumber, 2)
							$buffsID[$lBuffTargetNumber][2] = DllStructGetData($lBuff, "BuffID")
					EndSwitch
				EndIf
			Next
			For $i=1 To 8
				For $k=0 To 2
					If $buffsStatus[$i][$k] == $BUFFS_UNKNOWN Then
						buffsHide($i, $k)
					EndIf
				Next
			Next
		EndIf
		#endregion

		#region health
		If $health Then
			If DllStructGetData($lTgt, "Type")==0xDB Then
				$lTgtHP = DllStructGetData($lTgt, "HP")
				$lTgtMaxHP = DllStructGetData($lTgt, "MaxHP")
					GuiCtrlUpdateData($healthGuiLabelHP, StringFormat("%.1f", $lTgtHP*100))
					If $lTgtMaxHP <> 0 Then
						GuiCtrlUpdateData($healthGuiLabelMaxHP, Round($lTgtHP*$lTgtMaxHP)&" / "&$lTgtMaxHP)
					Else
						GuiCtrlUpdateData($healthGuiLabelMaxHP, "- / -")
					EndIf
					If $lTgtHP >= 0.9 Then
						healthUpdateColor($COLOR_HEALTH_HIGH)
					ElseIf $lTgtHP >= 0.5 Then
						healthUpdateColor($COLOR_HEALTH_MIDDLE)
					Else
						healthUpdateColor($COLOR_HEALTH_LOW)
					EndIf
			Else
				GuiCtrlUpdateData($healthGuiLabelHP, "-")
				GuiCtrlUpdateData($healthGuiLabelMaxHP, "- / -")
				healthUpdateColor($COLOR_GREY)
			EndIf
		EndIf
		#endregion health

		#region distance
		If $distance Then
			If DllStructGetData($lTgt, "Type")==0xDB Then
				distanceUpdateColor($COLOR_DISTANCE)
				Local $lDistance = GetDistance($lMe, $lTgt)
				If $lDistance < $RANGE_ADJACENT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_ADJACENT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Adjacent")
				ElseIf $lDistance < $RANGE_NEARBY Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_NEARBY * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Nearby")
				ElseIf $lDistance < $RANGE_AREA Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_AREA * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "In the Area")
				ElseIf $lDistance < $RANGE_EARSHOT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_EARSHOT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Earshot")
				ElseIf $lDistance < $RANGE_SPELLCAST Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_SPELLCAST * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Cast Range")
				ElseIf $lDistance < $RANGE_SPIRIT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_SPIRIT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Spirit Range")
				ElseIf $lDistance < $RANGE_COMPASS Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_COMPASS * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Compass")
				EndIf
			Else
				GuiCtrlUpdateData($distanceGuiLabelDist, "-")
				GuiCtrlUpdateData($distanceGuiLabelText, "-")
				distanceUpdateColor($COLOR_GREY)
			EndIf
		EndIf
		#endregion distance

		#region timer
		If $timer Then
			If GetMapLoading() <> $INSTANCETYPE_LOADING Then
				$sec = Int(GetInstanceUptime()/1000)
				If $sec <> $currentTime Then
					$currentTime = $sec
					$min = Int($sec/60)
					GUICtrlSetData($timerGuiLabel, Int($min/60)&":"&StringFormat("%02d", Mod($min, 60))&":"&StringFormat("%02d", Mod($sec, 60)))
					If GetMapID() == $MAP_ID_URGOZ And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
						$timerDefaultColor = False
						Local $temp = Mod($sec, 25)
						If $temp < 1 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPENING)
						ElseIf $temp < 13 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPEN)
						ElseIf $temp < 16 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_CLOSING)
						ElseIf $temp < 23 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_CLOSED)
						Else
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPENING)
						EndIf
					Else
						If Not $timerDefaultColor Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_TIMER)
							$timerDefaultColor = True
						EndIf
					EndIf
				EndIf
			Else
				GuiCtrlUpdateData($timerGuiLabel, "Loading")
			EndIf
		EndIf
		#endregion

		#region sf_macro
		If $sfmacro Then
			SkillsMacro()
		EndIf
		#endregion sf_macro

		hotkeyMainLoop($hDLL)

		pconsMainLoop($hDLL, $lMe, $lTgt, $lParty)

		dialogsMainLoop($hDLL)

		materialsMainLoop()

		#region autoupdate
		If $hDownload <> 0 Then
			If InetGetInfo($hDownload, 2) Then
				If @WorkingDir <> @ScriptDir Then FileChangeDir(@ScriptDir)
				Local $hFile = FileOpen($tmpFullPath)
				Local $serverVersion = FileRead($hFile)
				If $currentVersion <> $serverVersion And $serverVersion <> "" And $serverVersion <> 0 Then
					Local $changeLog = BinaryToString(InetRead($Host&"changelog.txt", 1))
					Local $height = Int(StringLeft($changeLog, 4))
					Local $msg = StringRight($changeLog, StringLen($changeLog)-4)
					If MyGuiMsgBox(4,  "GWToolbox - update?", "A new version ("&$serverVersion&") is available, would you like to download it?" & @CRLF & @CRLF & @CRLF & _
														"Changelog:" & @CRLF & $msg, $mainGui, 350, IsInt($height) ? $height : 200) == 6 Then
						; get the new executable
						InetGet($Host&"GWToolbox.exe", @ScriptDir&"/tmp.exe", 1)
						; make batch file
						Local $hBat = FileOpen(@ScriptDir&"/update.bat", 2)
						If $hBat = -1 Then
							MyGuiMsgBox(0, "GWToolbox", "Error while updating, please manually download the new version")
							exitProgram()
						EndIf
						; 	:begin
;~ 						;	del "@scriptfullpath"
;~ 						;	If exist "@scriptfullpath" goto begin
;~ 						;	copy "@scriptdir\tmp.exe" "@scriptdir\GWToolbox.exe"
;~ 						;	del "@scriptdir\tmp.exe"
;~ 						;	start "Windows Command Prompt" "@scriptdir\GWToolbox.exe"
;~ 						;	del "@scriptdir\update.bat"
;~ 						FileWrite($hBat, _
;~ 							":begin" & @CRLF & _
;~ 							'del "' & @ScriptFullPath & '"' & @CRLF & _
;~ 							'If exist "' & @ScriptFullPath & '" goto begin' & @CRLF & _
;~ 							'copy "'&@ScriptDir&'\tmp.exe" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& _
;~ 							'del "' & @ScriptDir & '\tmp.exe"' & @CRLF & _
;~ 							'start "Windows Command Prompt" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& _
;~ 							'del "' & @ScriptDir & '\update.bat"')
						FileWrite($hBat, ":begin" & @CRLF & 'del "' & @ScriptFullPath & '"' & @CRLF & 'If exist "' & @ScriptFullPath & '" goto begin' & @CRLF & 'copy "'&@ScriptDir&'\tmp.exe" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& 'del "' & @ScriptDir & '\tmp.exe"' & @CRLF & 'start "Windows Command Prompt" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& 'del "' & @ScriptDir & '\update.bat"')
						FileClose($hBat)
						FileClose($hFile)
						FileDelete($tmpFullPath)
						InetClose($hDownload)
						Sleep(50)
						Run(@ScriptDir&"\update.bat", @ScriptDir)
						exitProgram()
					EndIf
				EndIf
				FileClose($hFile)
				FileDelete($tmpFullPath)
				InetClose($hDownload)
				$hDownload = 0
			EndIf
		EndIf
		#endregion autoupdate

		Sleep(5)
	WEnd
EndFunc   ;==>mainLoop


Func SkillsMacro()
	If Not GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then Return
	Local $lMe = GetAgentByID(-2)
	If BitAND(DllStructGetData($lMe, 'Effects'), 0x0010) > 0 Then Return	; GetIsDead(-2)
	If DllStructGetData($lMe, 'Skill') <> 0 Then Return ; GetIsCasting(-2)
	Local $lSKillBar = GetSkillbar(0)
	If GUICtrlRead($sfmacroEmoMode) == $GUI_CHECKED Then
		Local $energyMissing = Int((1 - DllStructGetData($lMe, 'EnergyPercent')) * DllStructGetData($lMe, 'MaxEnergy'))
		If GetHeroProfession(0)==$PROF_ELEMENTALIST And $energyMissing > 10 Then
			Local $haveER = DllStructGetData(GetEffect($SKILL_ID_ETHER_RENEWAL), "SkillID") == $SKILL_ID_ETHER_RENEWAL
			Local $lSkillIDs[8]
			For $i=0 To 7
				$lSkillIDs[$i] = DllStructGetData($lSKillBar, "ID" & $i + 1)
			Next

			; Ether renewal
			For $i=0 To 7
				If $lSkillIDs[$i] == $SKILL_ID_ETHER_RENEWAL And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
					UseSkill($i+1, -2)
					pingSleep()
					Return
				EndIf
			Next

			If $haveER Then
				; spirit bond
				For $i=0 To 7
					If $lSkillIDs[$i] == $SKILL_ID_SPIRIT_BOND And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
						UseSkill($i+1, -2)
						pingSleep()
						Return
					EndIf
				Next

				; burning speed
				For $i=0 To 7
					If $lSkillIDs[$i] == $SKILL_ID_BURNING_SPEED And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
						UseSkill($i+1, -2)
						pingSleep()
						Return
					EndIf
				Next
			EndIf
		EndIf
	Else
		For $i = 0 To 7 Step 1
			If $sfmacroSkillsToUse[$i] Then
				If DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
					useSkill($i + 1, -1)
					pingSleep()
					Return
				EndIf
			EndIf
		Next
	EndIf
EndFunc

#region timer
Func toggleTimer()
	$timer = Not $timer
	If $timer Then
		Local $x = IniRead($iniFullPath, "timer", "x", -1)
		Local $y = IniRead($iniFullPath, "timer", "y", -1)
		$timerGui = GUICreate("", 150, 40, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($timerGui, 10)
		$timerGuiLabel = GUICtrlCreateLabel("", 0, 0, 150, 40, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont($timerGuiLabel, 26)
		GUICtrlSetColor($timerGuiLabel, $COLOR_TIMER)
		GUISetBkColor($COLOR_BLACK)
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "timerMoved")
		WinSetTrans($timerGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($timerGui)
	EndIf
	IniWrite($iniFullPath, "timer", "Active", $timer)
EndFunc

Func timerMoved()
	If $timer Then
		Local $pos = WinGetPos($timerGui)
		IniWrite($iniFullPath, "timer", "x", $pos[0])
		IniWrite($iniFullPath, "timer", "y", $pos[1])
	EndIf
EndFunc
#endregion timer

#region health
Func toggleHealth()
	$health = Not $health
	If $health Then
		Local $x = IniRead($iniFullPath, "health", "x", -1)
		Local $y = IniRead($iniFullPath, "health", "y", -1)
		$healthGui = GUICreate("", 105, 60, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($healthGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_HEALTH_HIGH)
		$healthGuiLabelHP = 	GUICtrlCreateLabel("", 3, 	0, 	85, 40, $SS_RIGHT, $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 26, 500)
		$healthGuiLabelPerc = 	GUICtrlCreateLabel("%",90, 	0, 	15, 40, BitOR($SS_LEFT, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 12)
		$healthGuiLabelMaxHP = 	GUICtrlCreateLabel("", 3, 	40, 105,20, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 12)
		$healthGuiCurrentColor = $COLOR_HEALTH_HIGH
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "healthMoved")
		WinSetTrans($healthGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($healthGui)
	EndIf
	IniWrite($iniFullPath, "health", "Active", $health)
EndFunc

Func healthMoved()
	If $health Then
		Local $pos = WinGetPos($healthGui)
		IniWrite($iniFullPath, "health", "x", $pos[0])
		IniWrite($iniFullPath, "health", "y", $pos[1])
	EndIf
EndFunc

Func healthUpdateColor($color)
	If $healthGuiCurrentColor <> $color Then
		GUICtrlSetColor($healthGuiLabelHP, $color)
		GUICtrlSetColor($healthGuiLabelMaxHP, $color)
		GUICtrlSetColor($healthGuiLabelPerc, $color)
		$healthGuiCurrentColor = $color
	EndIf
EndFunc
#endregion

#region distance
Func toggledistance()
	$distance = Not $distance
	If $distance Then
		Local $x = IniRead($iniFullPath, "distance", "x", -1)
		Local $y = IniRead($iniFullPath, "distance", "y", -1)
		$distanceGui = GUICreate("", 85, 50, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($distanceGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_DISTANCE)
		$distanceGuiLabelDist = GUICtrlCreateLabel("", 0, 0, 80, 30, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 24)
		$distanceGuiLabelPerc = GUICtrlCreateLabel("%", 70, 0, 15, 30, BitOR($SS_LEFT, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 10)
		$distanceGuiLabelText = GUICtrlCreateLabel("", 0, 30, 85, 20, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 10)
		$distanceGuiCurrentColor = $COLOR_DISTANCE
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "distanceMoved")
		WinSetTrans($distanceGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($distanceGui)
	EndIf
	IniWrite($iniFullPath, "distance", "Active", $distance)
EndFunc

Func distanceUpdateColor($color)
	If $distanceGuiCurrentColor <> $color Then
		GUICtrlSetColor($distanceGuiLabelDist, $color)
		GUICtrlSetColor($distanceGuiLabelPerc, $color)
		GUICtrlSetColor($distanceGuiLabelText, $color)
		$distanceGuiCurrentColor = $color
	EndIf
EndFunc

Func distanceMoved()
	If $distance Then
		Local $pos = WinGetPos($distanceGui)
		IniWrite($iniFullPath, "distance", "x", $pos[0])
		IniWrite($iniFullPath, "distance", "y", $pos[1])
	EndIf
EndFunc
#endregion distance

#region party
Func toggleParty()
	$party = Not $party
	If $party Then
		Local $lWidth = 30
		Local $lHeight = 22
		Local $x = IniRead($iniFullPath, "party", "x", -1)
		Local $y = IniRead($iniFullPath, "party", "y", -1)
		$partyGui = GUICreate("", $lWidth, $lHeight*8, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($partyGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_PARTY)
		For $i=1 To 8
			$partyLabels[$i] = GUICtrlCreateLabel("-", 0, ($i-1)*$lHeight, $lWidth, $lHeight, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
				GUICtrlSetFont(-1, 12)
		Next
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "partyMoved")
		WinSetTrans($partyGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($partyGui)
	EndIf
	IniWrite($iniFullPath, "party", "Active", $party)
EndFunc

Func partyMoved()
	If $party Then
		Local $pos = WinGetPos($partyGui)
		IniWrite($iniFullPath, "party", "x", $pos[0])
		IniWrite($iniFullPath, "party", "y", $pos[1])
	EndIf
EndFunc
#endregion

#region buffs
Func toggleBuffs()
	$buffs = Not $buffs
	If $buffs Then
		Local $lSize = 22
		$buffsX = IniRead($iniFullPath, "buffs", "x", -1)
		$buffsY = IniRead($iniFullPath, "buffs", "y", -1)
		$buffsGui = GUICreate("", $lSize*3, $lSize*8, $buffsX, $buffsY, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlCreateLabel("", 0, 0, $lSize*3, $lSize*8, Default, $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetOnEvent(-1, "buffsClicked")
		For $i=1 To 8
			$buffsImages[$i][0] = GUICtrlCreatePic("", 0*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			$buffsImages[$i][1] = GUICtrlCreatePic("", 1*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			$buffsImages[$i][2] = GUICtrlCreatePic("", 2*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			If @Compiled Then
				_Resource_SetToCtrlID($buffsImages[$i][0], "BALTHSPIRIT_JPG")
				_Resource_SetToCtrlID($buffsImages[$i][1], "LIFEBOND_JPG")
				_Resource_SetToCtrlID($buffsImages[$i][2], "PROTBOND_JPG")
			Else
				GUICtrlSetImage($buffsImages[$i][0], "img/balthspirit.jpg")
				GUICtrlSetImage($buffsImages[$i][1], "img/lifebond.jpg")
				GUICtrlSetImage($buffsImages[$i][2], "img/protbond.jpg")
			EndIf
		Next
		For $i=1 To 8
			For $k=0 To 2
				$buffsImagesHidden[$i][$k] = False
				buffsHide($i, $k)
			Next
		Next
;~ 		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "buffsMoved") ; unfortunately this won't trigger since the GUI is covered by labels with an event on them
		WinSetTrans($buffsGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($buffsGui)
	EndIf
	IniWrite($iniFullPath, "buffs", "Active", $buffs)
EndFunc

Func buffsClicked()
	If Not $buffsDrop Then Return
	Local $mousePos = GUIGetCursorInfo($buffsGui)
	Local $lBuffNo = Int(Floor($mousePos[0] / 22))
	Local $lPlayer = Int(Floor($mousePos[1] / 22)) + 1
	If $buffsStatus[$lPlayer][$lBuffNo] == $BUFFS_VISIBLE Then
		SendPacket(0x8, 0x22, $buffsID[$lPlayer][$lBuffNo])
		Sleep(10)
	EndIf
	buffsMoved()
EndFunc

Func buffsShow($aPlayer, $aBuffNo)
	If $buffsImagesHidden[$aPlayer][$aBuffNo] Then
		GUICtrlSetState($buffsImages[$aPlayer][$aBuffNo], $GUI_SHOW)
		$buffsImagesHidden[$aPlayer][$aBuffNo] = False
	EndIf
	$buffsStatus[$aPlayer][$aBuffNo] = $BUFFS_VISIBLE
EndFunc

Func buffsHide($aPlayer, $aBuffNo)
	If Not $buffsImagesHidden[$aPlayer][$aBuffNo] Then
		GUICtrlSetState($buffsImages[$aPlayer][$aBuffNo], $GUI_HIDE)
		$buffsImagesHidden[$aPlayer][$aBuffNo] = True
	EndIf
	$buffsStatus[$aPlayer][$aBuffNo] = $BUFFS_HIDDEN
EndFunc

Func buffsMoved()
	If $buffs Then
		Local $pos = WinGetPos($buffsGui)
		If $pos[0] <> $buffsX Then IniWrite($iniFullPath, "buffs", "x", $pos[0])
		If $pos[1] <> $buffsY Then IniWrite($iniFullPath, "buffs", "y", $pos[1])
	EndIf
EndFunc
#endregion buffs

#region GuiEventHandlers
Func tabEventHandler()
	If Not ($currentTab == $TAB_NONE) Then
		GUICtrlSetBkColor($tabButtons[$currentTab], $COLOR_BLACK)
		GUISetState(@SW_HIDE, $tabMainPanel[$currentTab])
		GUISetState(@SW_HIDE, $tabLeftPanel[$currentTab])

		If $tabButtons[$currentTab] == @GUI_CtrlId Then
			$currentTab = $TAB_NONE
			Return
		EndIf
	EndIf

	For $i = 0 To $tabCount-1
		If @GUI_CtrlId == $tabButtons[$i] Then
			GUICtrlSetBkColor($tabButtons[$i], $COLOR_BLACK)
			GUISetState(@SW_SHOW, $tabMainPanel[$i])
			GUISetState(@SW_SHOW, $tabLeftPanel[$i])
			$currentTab = $i
			ExitLoop
		EndIf
	Next
EndFunc

; note: 0x00780000 is wheel UP, 0xFF880010 is wheel DOWN (on $wParam)
Func mouseWheelHandler($hWnd, $MsgID, $wParam, $lParam)
	If $currentTab == $TAB_NONE Then Return

	Local $lAmount = 0
	If $wParam = 0x00780000 Then
		$lAmount = -20
	ElseIf $WParam = 0xFF880000 Then
		$lAmount = 20
	EndIf

	If $tabScrollPos[$currentTab] + $lAmount < 0 Then Return
	If $tabScrollPos[$currentTab] + $lAmount > $tabScrollMax[$currentTab] Then Return

	_GUIScrollBars_ScrollWindow($tabMainPanel[$currentTab], 0, -$lAmount)
	$tabScrollPos[$currentTab] += $lAmount
	Return
EndFunc

Func MyGuiCtrlMoveBy($hWindowID, $hControlID, $XAmount, $YAmount)
	Local $lPos = ControlGetPos($hWindowID, "", $hControlID)
	GUICtrlSetPos($hControlID, $lPos[0] + $XAmount, $lPos[1] + $YAmount, $lPos[2], $lPos[3])
EndFunc

Func mainGuiEventHandler()
	Switch @GUI_CtrlId
		Case $dragButton
			Local $pos = WinGetPos($mainGui)
			IniWrite($iniFullPath, "display", "x",$pos[0])
			IniWrite($iniFullPath, "display", "y", $pos[1])
		Case Else
			MyGuiMsgBox(0, "error", "not yet implemented"&@CRLF&"Please tell someone about it")
	EndSwitch
EndFunc
#endregion GuiEventHandlers


Func MyGuiCtrlCreateTab($sText, $iY, $sIcon = "")
	Local $lColor = $COLOR_WHITE
	Local $lBgColor = $COLOR_BLACK
	Local $lStyle = BitOR($SS_LEFT, $SS_CENTERIMAGE)
	Local $lStyleEx = -1
    GuiCtrlCreateRect(0, $iY + $TabButtonHeight - 1, $MainWidth, 1)
	Local $lWidth = $sText == "Pcons" ? $MainWidth - 20 : $MainWidth
	Local $ret = GUICtrlCreateLabel("        " & $sText, 0, $iY, $lWidth, $TabButtonHeight-1, $lStyle, $lStyleEx)
	GUICtrlSetBkColor(-1, $lBgColor)
	If $sIcon Then
		Local $lIcon = GUICtrlCreatePic("", 5, $iY+1, 24, 24)
		If @Compiled Then
			_Resource_SetToCtrlID($lIcon, $sIcon & "_JPG")
		Else
			GUICtrlSetImage($lIcon, "img/" & $sIcon & ".jpg")
		EndIf
		GUICtrlSetState($lIcon, $GUI_DISABLE)
	EndIf
    Return $ret
EndFunc

#region initialization
Func MyInitialize()
	Local $lGwProcList = ProcessList("gw.exe")
	Local $lToolboxProcList = ProcessList($GWToolbox&".exe")
	Local $lInitParameter

	; exit conditions:
	If $lGwProcList[0][0] == 0 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: Guild Wars is not running")
		exitProgram()
	EndIf
	If $lGwProcList[0][0] == 1 And $lToolboxProcList[0][0] == 2 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: GWToolbox is already running")
		exitProgram()
	EndIf

	Local $lActiveChars = ScanGW()

	Switch $lActiveChars[0]
		Case 0
			MyGuiMsgBox(0, $GWToolbox, "Error: You are not logged in any gw character. Please log in with a character")
			exitProgram()
		Case 1
			$lInitParameter = $lActiveChars[1]
		Case Else
			$lInitParameter = selectClient($lActiveChars)
	EndSwitch

	Local $lInitRet = Initialize($lInitParameter, False, False, False)

	If $lInitRet == 0 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: Unable to attach GWToolbox to Guild Wars")
		exitProgram()
	EndIf
EndFunc

Func selectClient($chars)
	Opt("GUIOnEventMode", False)
	Local $clientSelectionGui = GUICreate("GWToolbox", 300, 150, Default, Default, $WS_POPUP)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)

	Local $lCharString = ""
	For $i=1 To $chars[0]
		$lCharString = $lCharString & "|" & $chars[$i]
	Next

	GUICtrlCreateLabel("Select Client...", 0, 0, 300, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lCombo = GUICtrlCreateCombo("Select GW Client", 20, 50, 260, 25, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lCharString)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
	Local $okButton = MyGuiCtrlCreateButton("Ok", 20, 110, 110, 25)
	Local $cancelButton = MyGuiCtrlCreateButton("Cancel", 170, 110, 110, 25)

	GUISetState(@SW_SHOW)

	Local $lSelectedChar = ""

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $cancelButton
				Exit
			Case $okButton
				$lSelectedChar = GUICtrlRead($lCombo)
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($clientSelectionGui)
	Opt("GUIOnEventMode", True)
	Return $lSelectedChar
EndFunc
#endregion initialization

Func ArrayGetCommonElements($aArrA, $aArrB)
	Local $lReturnArray[1] = [0]
	For $i=1 To $aArrA[0]
		For $j=1 To $aArrB[0]
			If $aArrA[$i] == $aArrB[$j] Then
				$lReturnArray[0] += 1
				ReDim $lReturnArray[$lReturnArray[0] + 1]
				$lReturnArray[$lReturnArray[0]] = $aArrA[$i]
			EndIf
		Next
	Next
	Return $lReturnArray
EndFunc


Func minimizeProgram()
	GUISetState(@SW_MINIMIZE, $mainGui)
EndFunc

; Actually I don't care about the parameter, it's just a hack to allow Exit(somefunc())
; I know, this is horrible, I dont give an exit code and I don't close stuff. But windows doesn't care about the exit message, and AutoIt closes automatically all open resources... in theory.
Func exitProgram($param = 0)
	Exit 0
EndFunc

mainLoop()
