#include <myguifuncs.au3>
#include <lib/GwConstants.au3>
#include <lib/GWA2.au3>
#include <lib/Queue.au3>


; Gui Variables
Local $matsInput, $matsCombo, $matsBuyButton
Local $consInput, $consLabel, $consBuyButton
Local $scrollsInput, $scrollsLabel, $scrollsBuyButton
Local $pstonesInput, $pstonesLabel, $pstonesBuyButton
Local $logLabel, $priceCheckButton, $helpButton, $cancelButton

; Price check variables
Local $matsTimer = TimerInit(), $matsRequested = False
Local $lLastTraderCostID = 0

Local Enum $MAT_IRON = 0, $MAT_DUST, $MAT_BONES, $MAT_FEATHERS, $MAT_FIBERS, $MAT_GRANITE
Global $matCost[6]
For $i = 0 To 5
	$matCost[$i] = 0
Next
Global $matModelID[6]
$matModelID[$MAT_IRON] = $MODELID_IRON
$matModelID[$MAT_DUST] = $MODELID_DUST
$matModelID[$MAT_BONES] = $MODELID_BONES
$matModelID[$MAT_FEATHERS] = $MODELID_FEATHERS
$matModelID[$MAT_FIBERS] = $MODELID_FIBERS
$matModelID[$MAT_GRANITE] = $MODELID_GRANITE

; Action queue variables
Local Enum $ACTION_BUY, $ACTION_BUY_ID, $ACTION_PRICECHECK
Local $queue = Queue()

Func materialsBuildUI()
	Local $y = 5
	Local $labelX = 25
	Local $inputX = 35
	Local $comboX = 70
	Local $buttonX = 230
	GUICtrlCreateGroup("Manual", 10, $y, 280, 50)
	GUICtrlSetFont(-1, 9)
	GUICtrlCreateLabel("#", $labelX, $y+23)
	$matsInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
	GUICtrlSetColor(-1, $COLOR_BLACK)
	$matsCombo = GUICtrlCreateCombo("", $comboX, $y+19, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
	GUICtrlSetBkColor(-1, $COLOR_BLACK)
	Local $lMatsString = ""
	For $i=1 To $MATS_NAME[0]
		$lMatsString = $lMatsString & "|" & $MATS_NAME[$i]
	Next
	GUICtrlSetData(-1, $lMatsString)
	$matsBuyButton = MyGuiCtrlCreateButton("Buy", $buttonX, $y+20, 50, 20)
	GUICtrlSetOnEvent(-1, "matsEventHandler")

	$y+=52
	GUICtrlCreateGroup("Consets", 10, $y, 280, 50)
	GUICtrlSetFont(-1, 9)
	GUICtrlCreateLabel("#", $labelX, $y+23)
	$consInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
	GUICtrlSetColor(-1, $COLOR_BLACK)
	$consLabel = GUICtrlCreateLabel("-", $comboX, $y+23, 150, 20, $SS_CENTER)
	$consBuyButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)
	GUICtrlSetOnEvent(-1, "matsEventHandler")

	$y+=52
	GUICtrlCreateGroup("Res Scrolls", 10, $y, 280, 50)
	GUICtrlSetFont(-1, 9)
	GUICtrlCreateLabel("#", $labelX, $y+23)
	$scrollsInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
	GUICtrlSetColor(-1, $COLOR_BLACK)
	$scrollsLabel = GUICtrlCreateLabel("-", $comboX, $y+23, 150, 20, $SS_CENTER)
	$scrollsBuyButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)
	GUICtrlSetOnEvent(-1, "matsEventHandler")

	$y+=52
	GUICtrlCreateGroup("Powerstones", 10, $y, 280, 50)
	GUICtrlSetFont(-1, 9)
	GUICtrlCreateLabel("#", $labelX, $y+23)
	$pstonesInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
	GUICtrlSetColor(-1, $COLOR_BLACK)
	$pstonesLabel = GUICtrlCreateLabel("-", $comboX, $y+23, 150, 20, $SS_CENTER)
	$pstonesBuyButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)
	GUICtrlSetOnEvent(-1, "matsEventHandler")

	$y+=52
	$logLabel = GUICtrlCreateLabel("idle", $labelX, $y+5, 160, 20)
	$y += 30
	$helpButton = MyGuiCtrlCreateButton("Help", 10, $y, 90, 25)
	GUICtrlSetOnEvent($helpButton, "helpMaterialsBuyer")
	GUICtrlSetTip($helpButton, "Click for help")

	$priceCheckButton = MyGuiCtrlCreateButton("Price Check", 105, $y, 90, 25)
	GUICtrlSetOnEvent(-1, "matsEventHandler")

	$cancelButton = MyGuiCtrlCreateButton("Cancel", 200, $y, 90, 25)
	GUICtrlSetOnEvent(-1, "matsEventHandler")
EndFunc


Func helpMaterialsBuyer()
	Local $lTitle = "Help: Materials Buyer"
	Local $lText = 	"You have to talk to the materials trader before using the materials buyer. You can even close the materials trader window"&@CRLF&@CRLF& _
					"The Materials Buyer will stop working if you talk with another trader or crafter NPC (even cons crafters). At that point you have to zone or change district in order to make it work again"&@CRLF&@CRLF& _
					"The Materials Buyer will be unable to buy materials or estimate price if you move while it's doing such actions."&@CRLF&@CRLF& _
					"When you click Price Check, the Materials Buyer will try to estimate prices."&@CRLF&@CRLF& _
					"Quantity specifies the number of trades OR the number of items to buy materials for. Ex.: You will get 50 bones if you put 5 and then buy bones; You will get materials for 1 conset if you put 1 and then buy mats for conset"
	MyGuiMsgBox(0, $lTitle, $lText, $mainGui, 500, 320, True)
EndFunc

Func materialsMainLoop()
	If (Not Queue_IsEmpty($queue)) And TimerDiff($matsTimer) > 500 Then
		Local $error = False
		Local $data = Queue_Peek($queue)
		Local $lAction = $data[0]
		Local $lMatID = $data[1]
		Local $lMatModelID = $lAction == $ACTION_BUY_ID ? $data[1] : $matModelID[$data[1]]

		GUICtrlSetData($logLabel, "Working... " & (Queue_Count($queue) * 2 + ($matsRequested ? -1 : 0)))

		If Not $matsRequested Then
			Local $lRet = TraderRequest($lMatModelID)
			If Not $lRet Then ; if error notify user and clear queue
				GUICtrlSetData($logLabel, "Error requesting quote")
				$error = True
			Else ; everything is fine, mats were requested
				$matsRequested = True
			EndIf
		Else
			If $lAction == $ACTION_PRICECHECK Then
				Local $lTraderCostValue = GetTraderCostValue()
				Local $lTraderCostID = GetTraderCostID()
				If $lTraderCostID == 0 Or $lTraderCostValue == 0 Or $lTraderCostID == $lLastTraderCostID Then
					GUICtrlSetData($logLabel, "Error computing cost")
					$error = True
				Else
					$lLastTraderCostID = $lTraderCostID

					$matCost[$lMatID] = $lTraderCostValue

					If $matCost[$MAT_DUST] > 0 And $matCost[$MAT_IRON] > 0 And $matCost[$MAT_BONES] > 0 And $matCost[$MAT_FEATHERS] > 0 Then
						GUICtrlSetData($consLabel, "Cost: " & ($matCost[$MAT_DUST]*10 + $matCost[$MAT_IRON]*10 + $matCost[$MAT_BONES]*5 + $matCost[$MAT_FEATHERS]*5 + 750) / 1000 & " k")
					Else
						GUICtrlSetData($consLabel, "-")
					EndIf

					If $matCost[$MAT_FIBERS] > 0 And $matCost[$MAT_BONES] > 0 Then
						GUICtrlSetData($scrollsLabel, "Cost: " & ($matCost[$MAT_FIBERS] * 2.5 + $matCost[$MAT_BONES] * 2.5 + 250) / 1000 & " k")
					Else
						GUICtrlSetData($scrollsLabel, "-")
					EndIf

					If $matCost[$MAT_GRANITE] > 0 And $matCost[$MAT_DUST] > 0 Then
						GUICtrlSetData($pstonesLabel, "Cost: " & ($matCost[$MAT_GRANITE] * 10 + $matCost[$MAT_DUST] * 10 + 1000) / 1000 & " k")
					Else
						GUICtrlSetData($pstonesLabel, "-")
					EndIf

					Queue_Dequeue($queue)
				EndIf

			ElseIf $lAction == $ACTION_BUY Or $lAction == $ACTION_BUY_ID Then
				If GetGoldCharacter() < GetTraderCostValue() Then ; check if we have enough money
					GUICtrlSetData($logLabel, "Error buying: not enough money!")
					$error = True
				Else ; go buy
					If Not TraderBuy() Then
						GUICtrlSetData($logLabel, "Error buying")
						$error = True
					Else
						Queue_Dequeue($queue)
					EndIf
				EndIf
			EndIf

			$matsRequested = False
		EndIf

		$matsTimer = TimerInit()
		If $error Then
			$matsRequested = False
			Queue_Clear($queue)
		Else
			If Queue_IsEmpty($queue) Then
				GUICtrlSetData($logLabel, "Done")
				$matsRequested = False
			EndIf
		EndIf
	EndIf
EndFunc


Func matsEventHandler()
	Switch @GUI_CtrlId
		Case $matsBuyButton
			Local $lMatID = getMatIDByName(GUICtrlRead($matsCombo))
			Local $lMatQuantity = Int(GUICtrlRead($matsInput))
			If $lMatQuantity <= 0 Then Return GUICtrlSetData($logLabel, "Input a positive number!")
			If $lMatID == -1 Then Return GUICtrlSetData($logLabel, "Select a material first!")
			Local $data[2] = [$ACTION_BUY_ID, $lMatID]
			For $j = 1 To $lMatQuantity
				Queue_Enqueue($queue, $data)
			Next

		Case $consBuyButton
			Local $lMatQuantity = Int(GUICtrlRead($consInput))
			If $lMatQuantity <= 0 Then Return GUICtrlSetData($logLabel, "Input a positive number!")
			Local $data1[2] = [$ACTION_BUY, $MAT_IRON]
			Local $data2[2] = [$ACTION_BUY, $MAT_DUST]
			Local $data3[2] = [$ACTION_BUY, $MAT_BONES]
			Local $data4[2] = [$ACTION_BUY, $MAT_FEATHERS]
			For $i=1 To $lMatQuantity ; for each cons
				For $j = 1 To 10
					Queue_Enqueue($queue, $data1)
					Queue_Enqueue($queue, $data2)
				Next
				For $j = 1 To 5
					Queue_Enqueue($queue, $data3)
					Queue_Enqueue($queue, $data4)
				Next
			Next

		Case $scrollsBuyButton
			Local $lMatQuantity = Int(GUICtrlRead($scrollsInput))
			If $lMatQuantity <= 0 Then Return GUICtrlSetData($logLabel, "Input a positive number!")
			Local $data1[2] = [$ACTION_BUY, $MAT_FIBERS]
			Local $data2[2] = [$ACTION_BUY, $MAT_BONES]
			Local $data3[2] = [$ACTION_BUY, $MAT_FIBERS]
			Local $data4[2] = [$ACTION_BUY, $MAT_BONES]
			For $i=1 To $lMatQuantity ; for each scroll
				If Mod($i, 2) == 1 Then
					For $j = 1 To 3
						Queue_Enqueue($queue, $data1)
						Queue_Enqueue($queue, $data2)
					Next
				Else
					For $j = 1 To 2
						Queue_Enqueue($queue, $data3)
						Queue_Enqueue($queue, $data4)
					Next
				EndIf
			Next

		Case $pstonesBuyButton
			Local $lMatQuantity = Int(GUICtrlRead($pstonesInput))
			If $lMatQuantity <= 0 Then Return GUICtrlSetData($logLabel, "Input a positive number!")
			Local $data1[2] = [$ACTION_BUY, $MAT_GRANITE]
			Local $data2[2] = [$ACTION_BUY, $MAT_DUST]
			For $i=1 To $lMatQuantity ; for each pstone
				For $j = 1 To 10
					Queue_Enqueue($queue, $data1)
					Queue_Enqueue($queue, $data2)
				Next
			Next

		Case $priceCheckButton
			GUICtrlSetData($logLabel, "Checking prices...")
			Local $data[2] = [$ACTION_PRICECHECK, 0]
			For $i = 0 To 5
				$data[1] = $i
				Queue_Enqueue($queue, $data)
			Next

		Case $cancelButton
			If Not Queue_IsEmpty($queue) Then GUICtrlSetData($logLabel, "Cancelled")
			Queue_Clear($queue)
	EndSwitch
EndFunc

Func getMatIDByName($lMatName)
	For $i=1 To $MATS_NAME[0]
		If $lMatName == $MATS_NAME[$i] Then
			Return $MATS_ID[$i]
		EndIf
	Next
	Return -1
EndFunc
