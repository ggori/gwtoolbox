#cs
this file contains everything related to the pcons tab and pcons popping functionality

current pcons categories:
conset
red rock candy
blue rock candy
green rock candy
alcohol
slice of pumpkin pie
birthday cupcake
skalefin soup
panhai salad
candy apple
candy corn
golden egg
drake babob
war supplies
lunar fortunes
res scrolls
mobstoppers
city speedboosts
#ce

#include <GUIConstants.au3>
#include <GUIConstantsEx.au3>
#include <ColorConstants.au3>
#include <GuiComboBox.au3>
#include <Math.au3>

#include <lib/GWA2.au3>
#include <lib/GwConstants.au3>
#include <lib/GwFuncs.au3>
#include <myguifuncs.au3>
#include <globals.au3>
#include <hotkeyUtils.au3>

Global $pcons ; if the whole pcons feature is active or not
Global $pconsHotkey ; if the hotkey to enable / disable pcons is active
Global $pconsHotkeyHotkey ; the actual hotkey to enable / disable pcons
Global $CheckboxPCons ; the checkbox in the main interface

Global $AlcoholUsageTimer = TimerInit()
Global $AlcoholUsageCount = 0

; interface items
Global Const $pconsCount = 18
Global Enum $PCONS_CONS, $PCONS_ALCOHOL, $PCONS_RRC, $PCONS_BRC, $PCONS_GRC, $PCONS_PIE, $PCONS_CUPCAKE, $PCONS_APPLE, _
$PCONS_CORN, $PCONS_EGG, $PCONS_KABOB, $PCONS_WARSUPPLY, $PCONS_LUNARS, $PCONS_RES, $PCONS_SKALESOUP, $PCONS_MOBSTOPPERS, _
$PCONS_PANHAI, $PCONS_CITY
Global $pconsCheckbox[$pconsCount]
Global $pconsActive[$pconsCount]  ; status (active or not)

; ini names
Global $pconsIniSection = "pcons"
Global $pconsName[$pconsCount]
$pconsName[$PCONS_CONS] = "cons"
$pconsName[$PCONS_ALCOHOL] = "alcohol"
$pconsName[$PCONS_RRC] = "RRC"
$pconsName[$PCONS_BRC] = "BRC"
$pconsName[$PCONS_GRC] = "GRC"
$pconsName[$PCONS_PIE] = "pie"
$pconsName[$PCONS_CUPCAKE] = "cupcake"
$pconsName[$PCONS_APPLE] = "apple"
$pconsName[$PCONS_CORN] = "corn"
$pconsName[$PCONS_EGG] = "egg"
$pconsName[$PCONS_KABOB] = "kabob"
$pconsName[$PCONS_WARSUPPLY] = "warsupply"
$pconsName[$PCONS_LUNARS] = "lunars"
$pconsName[$PCONS_RES] = "res"
$pconsName[$PCONS_SKALESOUP] = "skalesoup"
$pconsName[$PCONS_MOBSTOPPERS] = "mobstoppers"
$pconsName[$PCONS_PANHAI] = "panhai"
$pconsName[$PCONS_CITY] = "city"

Global $pconsItemID[$pconsCount]
$pconsItemID[$PCONS_CONS] = -1
$pconsItemID[$PCONS_ALCOHOL] = -1
$pconsItemID[$PCONS_RRC] = $ITEM_ID_RRC
$pconsItemID[$PCONS_BRC] = $ITEM_ID_BRC
$pconsItemID[$PCONS_GRC] = $ITEM_ID_GRC
$pconsItemID[$PCONS_PIE] = $ITEM_ID_PIES
$pconsItemID[$PCONS_CUPCAKE] = $ITEM_ID_CUPCAKES
$pconsItemID[$PCONS_APPLE] = $ITEM_ID_APPLES
$pconsItemID[$PCONS_CORN] = $ITEM_ID_CORNS
$pconsItemID[$PCONS_EGG] = $ITEM_ID_EGGS
$pconsItemID[$PCONS_KABOB] = $ITEM_ID_KABOBS
$pconsItemID[$PCONS_WARSUPPLY] = $ITEM_ID_WARSUPPLIES
$pconsItemID[$PCONS_LUNARS] = -1
$pconsItemID[$PCONS_RES] = $ITEM_ID_RES_SCROLLS
$pconsItemID[$PCONS_SKALESOUP] = $ITEM_ID_SKALEFIN_SOUP
$pconsItemID[$PCONS_MOBSTOPPERS] = $ITEM_ID_MOBSTOPPER
$pconsItemID[$PCONS_PANHAI] = $ITEM_ID_PAHNAI_SALAD
$pconsItemID[$PCONS_CITY] = -1


; effects
Global Enum $pconsConsArmor = 1, $pconsConsGrail, $pconsConsEssence, $pconsRedrock, $pconsBluerock, $pconsGreenrock, _
			$pconsPie, $pconsCupcake, $pconsApple, $pconsCorn, $pconsEgg, $pconsKabob, $pconsWarSupply, $pconsLunars, $pconsSkaleSoup, $pconsMobstoppers, $pconsPahnai
Global $pconsEffects[18]
	$pconsEffects[0] = 17
	$pconsEffects[1] = $EFFECT_CONS_ARMOR
	$pconsEffects[2] = $EFFECT_CONS_GRAIL
	$pconsEffects[3] = $EFFECT_CONS_ESSENCE
	$pconsEffects[4] = $EFFECT_REDROCK
	$pconsEffects[5] = $EFFECT_BLUEROCK
	$pconsEffects[6] = $EFFECT_GREENROCK
	$pconsEffects[7] = $EFFECT_PIE
	$pconsEffects[8] = $EFFECT_CUPCAKE
	$pconsEffects[9] = $EFFECT_APPLE
	$pconsEffects[10] = $EFFECT_CORN
	$pconsEffects[11] = $EFFECT_EGG
	$pconsEffects[12] = $EFFECT_KABOBS
	$pconsEffects[13] = $EFFECT_WARSUPPLIES
	$pconsEffects[14] = $EFFECT_LUNARS
	$pconsEffects[15] = $EFFECT_SKALE_VIGOR
	$pconsEffects[16] = $EFFECT_WEAKENED_BY_DHUUM
	$pconsEffects[17] = $EFFECT_PAHNAI_SALAD

Global $pconsCityEffects[5]
	$pconsCityEffects[0] = 4
	$pconsCityEffects[1] = $EFFECT_BLUE_DRINK
	$pconsCityEffects[2] = $EFFECT_CHOCOLATE_BUNNY
	$pconsCityEffects[3] = $EFFECT_CREME_BRULEE
	$pconsCityEffects[4] = $EFFECT_RED_BEAN_CAKE_FRUITCAKE

Global $pconsCityModels[7]
	$pconsCityModels[0] = 6
	$pconsCityModels[1] = $ITEM_ID_SUGARY_BLUE_DRINK
	$pconsCityModels[2] = $ITEM_ID_CHOCOLATE_BUNNY
	$pconsCityModels[3] = $ITEM_ID_FRUITCAKE
	$pconsCityModels[4] = $ITEM_ID_CREME_BRULEE
	$pconsCityModels[5] = $ITEM_ID_RED_BEAN_CAKE
	$pconsCityModels[6] = $ITEM_ID_JAR_OF_HONEY



Func pconsLoadIni()
	$pcons = (IniRead($iniFullPath, $pconsIniSection, "active", 		False) == "True")
	$pconsHotkey = (IniRead($iniFullPath,  $pconsIniSection, "hkActive", 		False) == "True")
	$pconsHotkeyHotkey = IniRead($iniFullPath, $pconsIniSection, "hotkey", "00")
	For $i = 0 To $pconsCount-1
		$pconsActive[$i] = (IniRead($iniFullPath, $pconsIniSection, $pconsName[$i], 	False) == "True")
	Next
EndFunc

Func pconsBuildUI()
	If $pcons Then GUICtrlSetState($CheckboxPCons, $GUI_CHECKED)

	Local $x = 10, $y = 7
	GUICtrlCreateTabItem("Cons")
	GUICtrlCreateGroup("Status", $x, $y, 130, 50)
	Global Const $pconsStatusLabel = GUICtrlCreateLabel("Waiting...", $x + 10, $y + 15, 110, 30, BitOR($SS_CENTER, $SS_CENTERIMAGE))
		GUICtrlSetFont(-1, 15)
		GUICtrlSetData($pconsStatusLabel, ($pcons) ? "Active" : "Disabled")
		GUICtrlSetColor($pconsStatusLabel, ($pcons) ? $COLOR_GREEN : $COLOR_RED)
	Global Const $pconsToggle = MyGuiCtrlCreateButton("Toggle Active", $x + 139, $y + 9, 100, 40)
		GUICtrlSetOnEvent(-1, "pconsToggleActive")

	GUICtrlCreateGroup("Toggle Hotkey:", $x + 248, $y, 130, 50)
	Global Const $pconsHotkeyActive = GUICtrlCreateCheckbox("Active", 258+$x, $y+25, 50, 17)
		GUICtrlSetOnEvent(-1, "pconsHotkeyToggleActive")
		GUICtrlSetFont(-1, 9.5)
		GUICtrlSetTip(-1, "Activate/Disable the hotkey to enable/disable the pcons feature")
		If $pconsHotkey Then GUICtrlSetState($pconsHotkeyActive, $GUI_CHECKED)
	Global $pconsHotkeyInput = MyGuiCtrlCreateButton("", $x + 313, $y+21, 57, 22)
		GUICtrlSetOnEvent($pconsHotkeyInput, "setHotkey")
		GUICtrlSetData($pconsHotkeyInput, IniRead($keysIniFullPath, "idToKey", $pconsHotkeyHotkey, ""))

	Global $pconsX = $x + 5
	Global $pconsY = $y + 55

	GUISetFont(10)
	$pconsCheckbox[$PCONS_CONS] = 		GUICtrlCreateCheckbox("", $pconsX, $pconsY, 190, 18)
		GUICtrlSetTip($pconsCheckbox[$PCONS_CONS], _
			"Warning:"&@CRLF& _
			"Cons will be used when all players have loaded into the instance and are alive,"&@CRLF& _
			"they must be in radar range and this will only occur within the first 60 seconds."&@CRLF& _
			"Make sure the right party size is detected")

	$pconsCheckbox[$PCONS_RRC] = 			GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+20, 190, 18)
	$pconsCheckbox[$PCONS_BRC] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+40, 190, 18)
	$pconsCheckbox[$PCONS_GRC] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+60, 190, 18)
	$pconsCheckbox[$PCONS_ALCOHOL] = 	GUICtrlCreateCheckbox("", $pconsX,	$pconsY+80, 190, 18)
		GUICtrlSetTip($pconsCheckbox[$PCONS_ALCOHOL], "The Toolbox will use most common kinds of alcohol"&@CRLF&"Note that it will use the first item found in inventory (starting from the backpack)")
	$pconsCheckbox[$PCONS_PIE] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+100,190, 18)
	$pconsCheckbox[$PCONS_CUPCAKE] = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+120,190, 18)
	$pconsCheckbox[$PCONS_SKALESOUP] = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+140,190, 18)
		GUICtrlSetTip($pconsCheckbox[$PCONS_SKALESOUP], "(+1 Health regeneration for 10 minutes)")
	$pconsCheckbox[$PCONS_PANHAI] = 	GUICtrlCreateCheckbox("", $pconsX,	$pconsY+160,190, 18)
		GUICtrlSetTip($pconsCheckbox[$PCONS_PANHAI], "(+20 Maximum Health for 10 minutes)")
	$pconsX = 215
	$pconsCheckbox[$PCONS_APPLE] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY, 	190, 18)
	$pconsCheckbox[$PCONS_CORN] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+20, 190, 18)
	$pconsCheckbox[$PCONS_EGG] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+40, 190, 18)
	$pconsCheckbox[$PCONS_KABOB] = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+60, 190, 18)
		GUICtrlSetTip(-1, "(+5 Armor for 5 minutes)")
	$pconsCheckbox[$PCONS_WARSUPPLY] = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+80, 190, 18)
	$pconsCheckbox[$PCONS_LUNARS] = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+100,190, 18)
		GUICtrlSetTip(-1, "It will use Lunar Fortunes until the Lunar Blessing is obtained")
	$pconsCheckbox[$PCONS_RES] = 		GUICtrlCreateCheckbox("", $pconsX,	$pconsY+120,190, 18)
		GUICtrlSetTip(-1, "It will use a res scroll whenever there is a dead party member in range")
	$pconsCheckbox[$PCONS_MOBSTOPPERS] =GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+140,190, 18)
		GUICtrlSetTip(-1, "It will use mobstopper when you have an alive skeleton of dhuum targeted and it's below 25% hp and it's in the area")
	$pconsCheckbox[$PCONS_CITY] = GUICtrlCreateCheckbox("", $pconsX,	$pconsY+160,190, 18)
		GUICtrlSetTip(-1, "It will use a Sugary Blue Drink, Chocolate Bunny, Fruitcake, Creme Brulee, Red Bean Cake or Jar of Honey when moving in a town or outpost")
		For $i = 0 To $pconsCount-1
			If $pconsActive[$i] Then GUICtrlSetState($pconsCheckbox[$i], $GUI_CHECKED)
			GUICtrlSetOnEvent($pconsCheckbox[$i], "pconsToggle")
		Next
	GUISetFont(8.5)

	Global Const $pconsPresetList = GUICtrlCreateCombo("", $x, $y + 234, 125, 20, $CBS_DROPDOWNLIST)
	Global Const $pconsPresetDefault = "Select Preset..."
		GUICtrlSetColor(-1, $COLOR_WHITE)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent(-1, "presetLoad")
		SetPresetCombo($pconsPresetList, $pconsPresetDefault)

	Global Const $pconsPresetNew = MyGuiCtrlCreateButton("Save...", $x + 135, $y + 235, 75, 22)
		GUICtrlSetOnEvent(-1, "presetSave")

	Global Const $pconsPresetDelete = MyGuiCtrlCreateButton("Delete...", $x + 220, $y + 235, 75, 22)
		GUICtrlSetOnEvent(-1, "presetDelete")

	Global Const $pconsScan = MyGuiCtrlCreateButton("Update", $x + 305, $y + 235, 75, 22)
		GUICtrlSetTip(-1, "Scan inventory")
		GUICtrlSetOnEvent(-1, "pconsScanInventory")
EndFunc


Local $pressedPcons = False

; timers for the pcons (prevent more than 1 pcon being used every time)
Local $pconsTimer[$pconsCount]
For $i = 0 To $pconsCount-1
	$pconsTimer[$i] = TimerInit()
Next
Local $currentLoadingState, $currentMap

; Variables that needs to be recomputed everytime
Local $pconsRetArray = GetHasEffects($pconsEffects)
Local $pconsRetArrayVerify = GetHasEffects($pconsEffects)
For $i=1 To $pconsRetArray[0]
	$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
Next

Local $pconsCityRetArray = GetHasEffects($pconsCityEffects)
Local $pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
For $i=1 To $pconsCityRetArray[0]
	$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
Next

Func pconsMainLoop($hDLL, Const ByRef $lMe, Const ByRef $lTgt, Const ByRef $lParty)
	Local $pressed

	If $pconsHotkey Then
		$pressed = _IsPressed($pconsHotkeyHotkey, $hDLL)
		If (Not $pressedPcons) And $pressed Then
			$pressedPcons = True
			pconsToggleActive()
			WriteChat("Pcons are now " & ($pcons ? "Enabled" : "Disabled"), $GWToolbox)
		ElseIf $pressedPcons And (Not $pressed) Then
			$pressedPcons = False
		EndIf
	EndIf

	; maploading for alcohol
	If GetMapLoading() <> $currentLoadingState Then
		$currentLoadingState = GetMapLoading()
		If $currentLoadingState == $INSTANCETYPE_EXPLORABLE Or $currentLoadingState == $INSTANCETYPE_OUTPOST Then
			$currentMap = GetMapID()
			pconsScanInventory()
			$AlcoholUsageCount = 0
		EndIf
	ElseIf GetMapID() <> $currentMap Then
		$currentMap = GetMapID()
		pconsScanInventory()
		$AlcoholUsageCount = 0
	EndIf

	If GetAlcoholTimeRemaining() < 0 Then
		$AlcoholUsageTimer = TimerInit()
		$AlcoholUsageCount = 0
	EndIf

	; get stuff
	If ($pcons) Then
		Switch GetMapLoading()
			Case $INSTANCETYPE_EXPLORABLE
				$pconsRetArray = GetHasEffects($pconsEffects)
				$pconsRetArrayVerify = GetHasEffects($pconsEffects)
				For $i=1 To $pconsRetArray[0]
					$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
				Next

			Case $INSTANCETYPE_OUTPOST
				$pconsCityRetArray = GetHasEffects($pconsCityEffects)
				$pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
				For $i=1 To $pconsCityRetArray[0]
					$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
				Next
		EndSwitch
	EndIf

	If $pcons Then
		If GetMapLoading()==$INSTANCETYPE_EXPLORABLE And (Not GetIsDead(-2)) And (DllStructGetData($lMe, "HP")>0) Then

			#region cons
			If $pconsActive[$PCONS_CONS] And GetInstanceUptime() < (60*1000) Then
				If $pconsRetArray[$pconsConsEssence] < 1000 And $pconsRetArray[$pconsConsArmor] < 1000 And $pconsRetArray[$pconsConsGrail] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_CONS]) > 5000 Then				; if timer isnt bad
						Local $lSize = GetPartySize()
						If $lSize > 0 And $lParty[0] == $lSize Then ; if the whole party is in range (and loaded)
							Local $everybodyAliveAndLoaded = True
							For $i=1 To $lParty[0] Step 1
								If DllStructGetData($lParty[$i], "HP") <= 0 Then		; check if everyone is alive
									$everybodyAliveAndLoaded = False
									ExitLoop
								EndIf
							Next
							If $everybodyAliveAndLoaded Then
								If UseItemByModelID($ITEM_ID_CONS_ESSENCE) And UseItemByModelID($ITEM_ID_CONS_GRAIL) And UseItemByModelID($ITEM_ID_CONS_ARMOR) Then
									$pconsTimer[$PCONS_CONS] = TimerInit()
								Else
									pconsScanInventory()
									If Not $pconsActive[$PCONS_CONS] Then WriteChat("[WARNING] Cannot find cons", $GWToolbox)
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region alcohol
			If $pconsActive[$PCONS_ALCOHOL] Then
				If GetAlcoholTimeRemaining() <= 60 Then
					If TimerDiff($pconsTimer[$PCONS_ALCOHOL]) > 5000 Then
						$AlcoholUsageCount += UseAlcohol()
						If GetAlcoholTimeRemaining() > 60 Then
							$pconsTimer[$PCONS_ALCOHOL] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_ALCOHOL] Then WriteChat("[WARNING] Cannot find Alcohol", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region Redrocks
			If $pconsActive[$PCONS_RRC] Then
				If $pconsRetArray[$pconsRedrock] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_RRC]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_RRC]) Then
							$pconsTimer[$PCONS_RRC] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_RRC] Then WriteChat("[WARNING] Cannot find Red Rocks", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region Bluerock
			If $pconsActive[$PCONS_BRC] Then
				If $pconsRetArray[$pconsBluerock] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_BRC]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_BRC]) Then
							$pconsTimer[$PCONS_BRC] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_BRC] Then WriteChat("[WARNING] Cannot find Blue Rocks", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region Greenrock
			If $pconsActive[$PCONS_GRC] Then
				If $pconsRetArray[$pconsGreenrock] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_GRC]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_GRC]) Then
							$pconsTimer[$PCONS_GRC] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_GRC] Then WriteChat("[WARNING] Cannot find Green Rocks", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region pie
			If $pconsActive[$PCONS_PIE] Then
				If $pconsRetArray[$pconsPie] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_PIE]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_PIE]) Then
							$pconsTimer[$PCONS_PIE] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_PIE] Then WriteChat("[WARNING] Cannot find Pumpkin Pies", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region cupcakes
			If $pconsActive[$PCONS_CUPCAKE] Then
				If $pconsRetArray[$pconsCupcake] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_CUPCAKE]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_CUPCAKE]) Then
							$pconsTimer[$PCONS_CUPCAKE] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_CUPCAKE] Then WriteChat("[WARNING] Cannot find Cupcakes", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region apples
			If $pconsActive[$PCONS_APPLE] Then
				If $pconsRetArray[$pconsApple] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_APPLE]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_APPLE]) Then
							$pconsTimer[$PCONS_APPLE] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_APPLE] Then WriteChat("[WARNING] Cannot find Candy Apples", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region corn
			If $pconsActive[$PCONS_CORN] Then
				If $pconsRetArray[$pconsCorn] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_CORN]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_CORN]) Then
							$pconsTimer[$PCONS_CORN] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_CORN] Then WriteChat("[WARNING] Cannot find Candy Corns", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region egg
			If $pconsActive[$PCONS_EGG] Then
				If $pconsRetArray[$pconsEgg] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_EGG]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_EGG]) Then
							$pconsTimer[$PCONS_EGG] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_EGG] Then WriteChat("[WARNING] Cannot find Golden Eggs", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region kabobs
			If $pconsActive[$PCONS_KABOB] Then
				If $pconsRetArray[$pconsKabob] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_KABOB]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_KABOB]) Then
							$pconsTimer[$PCONS_KABOB] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_KABOB] Then WriteChat("[WARNING] Cannot find Drake Kabobs", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region warsupplies
			If $pconsActive[$PCONS_WARSUPPLY] Then
				If $pconsRetArray[$pconsWarSupply] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_WARSUPPLY]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_WARSUPPLY]) Then
							$pconsTimer[$PCONS_WARSUPPLY] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_WARSUPPLY] Then WriteChat("[WARNING] Cannot find War Supplies", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region lunars
			If $pconsActive[$PCONS_LUNARS] Then
				If $pconsRetArray[$pconsLunars] == 0 Then
					If TimerDiff($pconsTimer[$PCONS_LUNARS]) > GetPing()+500 Then
						If UseItemByModelID($ITEM_ID_LUNARS_DRAGON) Then
							$pconsTimer[$PCONS_LUNARS] = TimerInit()
						ElseIf UseItemByModelID($ITEM_ID_LUNARS_SNAKE) Then
							$pconsTimer[$PCONS_LUNARS] = TimerInit()
						ElseIf UseItemByModelID($ITEM_ID_LUNARS_HORSE) Then
							$pconsTimer[$PCONS_LUNARS] = TimerInit()
						ElseIf UseItemByModelID($ITEM_ID_LUNARS_RABBIT) Then
							$pconsTimer[$PCONS_LUNARS] = TimerInit()
						ElseIf UseItemByModelID($ITEM_ID_LUNARS_SHEEP) Then
							$pconsTimer[$PCONS_LUNARS] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_LUNARS] Then WriteChat("[WARNING] Cannot find Lunars Fortunes", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region res
			If $pconsActive[$PCONS_RES] Then
				If TimerDiff($pconsTimer[$PCONS_RES]) > 250 Then
					For $i=1 To $lParty[0]
						If GetIsDead($lParty[$i]) Then
							If GetDistance($lParty[$i], -2) < $RANGE_EARSHOT Then
								If UseItemByModelID($pconsItemID[$PCONS_RES]) Then
									$pconsTimer[$PCONS_RES] = TimerInit()
								Else
									pconsScanInventory()
									If Not $pconsActive[$PCONS_RES] Then WriteChat("[WARNING] Cannot find Res Scrolls", $GWToolbox)
								EndIf
								ExitLoop
							EndIf
						EndIf
					Next
				EndIf
			EndIf
			#endregion
			#region skalesoup
			If $pconsActive[$PCONS_SKALESOUP] Then
				If $pconsRetArray[$pconsSkaleSoup] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_SKALESOUP]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_SKALESOUP]) Then
							$pconsTimer[$PCONS_SKALESOUP] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_SKALESOUP] Then WriteChat("[WARNING] Cannot find Skalefin Soup", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region Mobstoppers
			If $pconsActive[$PCONS_MOBSTOPPERS] Then
				If DllStructGetData($lTgt, "PlayerNumber") == $MODELID_SKELETON_OF_DHUUM Then
					If DllStructGetData($lTgt, "HP") > 0 And DllStructGetData($lTgt, "HP") < 0.25 Then
						If GetDistance($lMe, $lTgt) < 400 Then
							If $pconsRetArray[$pconsMobstoppers] == 0 Then
								If TimerDiff($pconsTimer[$PCONS_MOBSTOPPERS]) > 5000 Then
									If UseItemByModelID($pconsItemID[$PCONS_MOBSTOPPERS]) Then
										$pconsTimer[$PCONS_MOBSTOPPERS] = TimerInit()
									Else
										pconsScanInventory()
										If Not $pconsActive[$PCONS_MOBSTOPPERS] Then WriteChat("[WARNING] Cannot find Mobstoppers", $GWToolbox)
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
			#region Pahnai salad
			If $pconsActive[$PCONS_PANHAI] Then
				If $pconsRetArray[$pconsPahnai] < 1000 Then
					If TimerDiff($pconsTimer[$PCONS_PANHAI]) > 5000 Then
						If UseItemByModelID($pconsItemID[$PCONS_PANHAI]) Then
							$pconsTimer[$PCONS_PANHAI] = TimerInit()
						Else
							pconsScanInventory()
							If Not $pconsActive[$PCONS_PANHAI] Then WriteChat("[WARNING] Cannot find Pahnai Salad", $GWToolbox)
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
		EndIf
		If GetMapLoading()==$INSTANCETYPE_OUTPOST Then
			#region City Speedboost
			If $pconsActive[$PCONS_CITY] Then
				If DllStructGetData($lMe, "MoveX") > 0 Or DllStructGetData($lMe, "MoveX") Then

					Local $lShouldUse = True
					For $i=1 To $pconsCityRetArray[0]
						If $pconsCityRetArray[$i] > 1000 Then $lShouldUse = False
					Next

					If $lShouldUse Then
						If TimerDiff($pconsTimer[$PCONS_CITY]) > 5000 Then

							Local $lUsed = False
							For $i=1 To $pconsCityModels[0]
								If UseItemByModelID($pconsCityModels[$i]) Then
									$lUsed = True
									$pconsTimer[$PCONS_CITY] = TimerInit()
									ExitLoop
								EndIf
							Next

							If Not $lUsed Then
								pconsScanInventory()
								If Not $pconsActive[$PCONS_CITY] Then WriteChat("[WARNING] Cannot find city speedboosts", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf
			#endregion
		EndIf
	EndIf
EndFunc


Func GetAlcoholTimeRemaining()
	Return Round(60*$AlcoholUsageCount - TimerDiff($AlcoholUsageTimer)/1000)
EndFunc

Func UseAlcohol()
	Local $lItem
	For $lBag=1 To 4
		For $lSlot=1 To $BAG_SLOTS[$lBag]
			$lItem = GetItemBySlot($lBag, $lSlot)
			For $i=1 To $ITEM_ID_ALCOHOL_1[0]
				If DllStructGetData($lItem, "ModelID") == $ITEM_ID_ALCOHOL_1[$i] Then
					SendPacket(0x8, 0x77, DllStructGetData($lItem, "ID"))
					Return 1
				EndIf
			Next
			For $i=1 To $ITEM_ID_ALCOHOL_5[0]
				If DllStructGetData($lItem, "ModelID") == $ITEM_ID_ALCOHOL_5[$i] Then
					SendPacket(0x8, 0x77, DllStructGetData($lItem, "ID"))
					Return 4
				EndIf
			Next
		Next
	Next
	Return 0
EndFunc

Func pconsToggleActive()
	$pcons = Not $pcons

	IniWrite($iniFullPath, $pconsIniSection, "active", $pcons)
	GUICtrlSetData($pconsStatusLabel, $pcons ? "Active" : "Disabled")
	GUICtrlSetColor($pconsStatusLabel, $pcons ? $COLOR_GREEN : $COLOR_RED)
	GUICtrlSetState($CheckboxPCons, $pcons ? $GUI_CHECKED : $GUI_UNCHECKED)

	If ($pcons) Then
		Switch GetMapLoading()
			Case $INSTANCETYPE_EXPLORABLE
				$pconsRetArray = GetHasEffects($pconsEffects)
				$pconsRetArrayVerify = GetHasEffects($pconsEffects)
				For $i=1 To $pconsRetArray[0]
					$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
				Next

			Case $INSTANCETYPE_OUTPOST
				$pconsCityRetArray = GetHasEffects($pconsCityEffects)
				$pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
				For $i=1 To $pconsCityRetArray[0]
					$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
				Next
		EndSwitch
	EndIf
EndFunc

Func pconsToggle()
	Local $aState = GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED
	Switch @GUI_CtrlId
		Case $pconsCheckbox[$PCONS_CONS]
			If $aState Then
				If pconsFind($ITEM_ID_CONS_ESSENCE) And pconsFind($ITEM_ID_CONS_ARMOR) And pconsFind($ITEM_ID_CONS_GRAIL) Then
					$pconsActive[$PCONS_CONS] = True
					IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_CONS], True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsActive[$PCONS_CONS] = False
				IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_CONS], False)
			EndIf
		Case $pconsCheckbox[$PCONS_ALCOHOL]
			If $aState Then
				Local $lFound = False
				For $i=1 To $ITEM_ID_ALCOHOL_1[0]
					If pconsFind($ITEM_ID_ALCOHOL_1[$i]) Then
						$lFound = True
						ExitLoop
					EndIf
				Next
				If Not $lFound Then
					For $i=1 To $ITEM_ID_ALCOHOL_5[0]
						If pconsFind($ITEM_ID_ALCOHOL_5[$i]) Then
							$lFound = True
							ExitLoop
						EndIf
					Next
				EndIf
				If $lFound Then
					$pconsActive[$PCONS_ALCOHOL] = True
					IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_ALCOHOL], True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsActive[$PCONS_ALCOHOL] = False
				IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_ALCOHOL], False)
			EndIf
		Case $pconsCheckbox[$PCONS_LUNARS]
			If $aState Then
				If pconsFind($ITEM_ID_LUNARS_DRAGON) Or pconsFind($ITEM_ID_LUNARS_SNAKE) Or pconsFind($ITEM_ID_LUNARS_HORSE) Or pconsFind($ITEM_ID_LUNARS_RABBIT) Or pconsFind($ITEM_ID_LUNARS_SHEEP) Then
					$pconsActive[$PCONS_LUNARS] = True
					IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_LUNARS], True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsActive[$PCONS_LUNARS] = False
				IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_LUNARS], False)
			EndIf
		Case $pconsCheckbox[$PCONS_CITY]
			If $aState Then
				Local $lFound = False
				For $i=1 To $pconsCityModels[0]
					If pconsFind($pconsCityModels[$i]) Then
						$lFound = True
						ExitLoop
					EndIf
				Next
				If $lFound Then
					$pconsActive[$PCONS_CITY] = True
					IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_CITY], True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsActive[$PCONS_CITY] = False
				IniWrite($iniFullPath, $pconsIniSection, $pconsName[$PCONS_CITY], False)
			EndIf
		Case Else
			For $i = 0 To $pconsCount-1
				If @GUI_CtrlId == $pconsCheckbox[$i] Then
					If $aState Then
						If pconsFind($pconsItemID[$i]) Then
							IniWrite($iniFullPath, $pconsIniSection, $pconsName[$i], True)
							If GUICtrlRead($pconsCheckbox[$i]) == $GUI_UNCHECKED Then GUICtrlSetState($pconsCheckbox[$i], $GUI_CHECKED)
							$pconsActive[$i] = True
						Else
							If GUICtrlRead($pconsCheckbox[$i]) == $GUI_CHECKED Then GUICtrlSetState($pconsCheckbox[$i], $GUI_UNCHECKED)
							$pconsActive[$i] = False
						EndIf
					Else
						IniWrite($iniFullPath, $pconsIniSection, $pconsName[$i], False)
						If GUICtrlRead($pconsCheckbox[$i]) == $GUI_CHECKED Then GUICtrlSetState($pconsCheckbox[$i], $GUI_UNCHECKED)
						$pconsActive[$i] = False
					EndIf
					ExitLoop
				EndIf
			Next
	EndSwitch
EndFunc

Func pconsFind($aModelID)
	For $lBag=1 To 4
		For $lSlot=1 To $BAG_SLOTS[$lBag]
			If DllStructGetData(GetItemBySlot($lBag, $lSlot), "ModelID") == $aModelID Then
				Return True
			EndIf
		Next
	Next
	Return False
EndFunc

Func pconsScanInventory()
	Local $lPconsQuantity[$pconsCount]
	For $i = 0 To $pconsCount-1
		$lPconsQuantity[$i] = 0
	Next
	Local $lConsEssence=0, $lConsGrail=0, $lConsArmor=0
	Local $lItem, $lQuantity, $lItemID
	For $lBag=1 To 4 Step 1
		For $lSlot = 1 To $BAG_SLOTS[$lBag]
			$lItem = GetItemBySlot($lBag, $lSlot)
			$lQuantity = DllStructGetData($lItem, "Quantity")
			$lItemID = DllStructGetData($lItem, "ModelID")
			Switch $lItemID
				Case 0
					ContinueLoop
				Case $ITEM_ID_CONS_ESSENCE
					$lConsEssence += $lQuantity
				Case $ITEM_ID_CONS_GRAIL
					$lConsGrail += $lQuantity
				Case $ITEM_ID_CONS_ARMOR
					$lConsArmor += $lQuantity
				Case $ITEM_ID_ALCOHOL_1[1], $ITEM_ID_ALCOHOL_1[2], $ITEM_ID_ALCOHOL_1[3], $ITEM_ID_ALCOHOL_1[4], $ITEM_ID_ALCOHOL_1[5], $ITEM_ID_ALCOHOL_1[6], $ITEM_ID_ALCOHOL_1[7], $ITEM_ID_ALCOHOL_1[8]
					$lPconsQuantity[$PCONS_ALCOHOL] += $lQuantity
				Case $ITEM_ID_ALCOHOL_5[1], $ITEM_ID_ALCOHOL_5[2], $ITEM_ID_ALCOHOL_5[3], $ITEM_ID_ALCOHOL_5[4], $ITEM_ID_ALCOHOL_5[5], $ITEM_ID_ALCOHOL_5[6], $ITEM_ID_ALCOHOL_5[7]
					$lPconsQuantity[$PCONS_ALCOHOL] += 5*$lQuantity
				Case $ITEM_ID_LUNARS_DRAGON, $ITEM_ID_LUNARS_SNAKE, $ITEM_ID_LUNARS_HORSE, $ITEM_ID_LUNARS_RABBIT, $ITEM_ID_LUNARS_SHEEP
					$lPconsQuantity[$PCONS_LUNARS] += $lQuantity
				Case $pconsCityModels[1], $pconsCityModels[2], $pconsCityModels[3], $pconsCityModels[4], $pconsCityModels[5]
					$lPconsQuantity[$PCONS_CITY] += $lQuantity
				Case Else
					For $i = 0 To $pconsCount-1
						If $lItemID > 0 And $lItemID == $pconsItemID[$i] Then
							$lPconsQuantity[$i] += $lQuantity
							ExitLoop
						EndIf
					Next
			EndSwitch
		Next
	Next
	$lPconsQuantity[$PCONS_CONS] = _Min($lConsEssence, _Min($lConsArmor, $lConsGrail))
	For $i = 0 To $pconsCount-1
		$pconsActive[$i] = $pconsActive[$i] And $lPconsQuantity[$i] > 0
		GUICtrlSetState($pconsCheckbox[$i], $pconsActive[$i] ? $GUI_CHECKED : $GUI_UNCHECKED)
		Switch $i
			Case $PCONS_CONS, $PCONS_RRC, $PCONS_RES, $PCONS_MOBSTOPPERS
				pconsSetColor($i, $lPconsQuantity[$i], 5)
			Case $PCONS_BRC, $PCONS_PIE, $PCONS_CUPCAKE, $PCONS_APPLE, $PCONS_CORN, $PCONS_KABOB, $PCONS_SKALESOUP, $PCONS_PANHAI
				pconsSetColor($i, $lPconsQuantity[$i], 10)
			Case $PCONS_GRC
				pconsSetColor($i, $lPconsQuantity[$i], 15)
			Case $PCONS_EGG, $PCONS_WARSUPPLY, $PCONS_CITY
				pconsSetColor($i, $lPconsQuantity[$i], 20)
			Case $PCONS_ALCOHOL, $PCONS_LUNARS
				pconsSetColor($i, $lPconsQuantity[$i], 30)
		EndSwitch
	Next

	GUICtrlSetData($pconsCheckbox[$PCONS_CONS], "("&$lPconsQuantity[$PCONS_CONS]&") Conset")
	GUICtrlSetData($pconsCheckbox[$PCONS_RRC], "("&$lPconsQuantity[$PCONS_RRC]&") Red Rock Candy")
	GUICtrlSetData($pconsCheckbox[$PCONS_BRC], "("&$lPconsQuantity[$PCONS_BRC]&") Blue Rock Candy")
	GUICtrlSetData($pconsCheckbox[$PCONS_GRC], "("&$lPconsQuantity[$PCONS_GRC]&") Green Rock Candy")
	GUICtrlSetData($pconsCheckbox[$PCONS_ALCOHOL], "("&$lPconsQuantity[$PCONS_ALCOHOL]&"min) Alcohol")
	GUICtrlSetData($pconsCheckbox[$PCONS_PIE], "("&$lPconsQuantity[$PCONS_PIE]&") Slice of Pumpkin Pie")
	GUICtrlSetData($pconsCheckbox[$PCONS_CUPCAKE], "("&$lPconsQuantity[$PCONS_CUPCAKE]&") Birthday Cupcake")
	GUICtrlSetData($pconsCheckbox[$PCONS_APPLE], "("&$lPconsQuantity[$PCONS_APPLE]&") Candy Apple")
	GUICtrlSetData($pconsCheckbox[$PCONS_CORN], "("&$lPconsQuantity[$PCONS_CORN]&") Candy Corn")
	GUICtrlSetData($pconsCheckbox[$PCONS_EGG], "("&$lPconsQuantity[$PCONS_EGG]&") Golden Egg")
	GUICtrlSetData($pconsCheckbox[$PCONS_KABOB], "("&$lPconsQuantity[$PCONS_KABOB]&") Drake Kabob")
	GUICtrlSetData($pconsCheckbox[$PCONS_WARSUPPLY], "("&$lPconsQuantity[$PCONS_WARSUPPLY]&") War Supplies")
	GUICtrlSetData($pconsCheckbox[$PCONS_LUNARS], "("&$lPconsQuantity[$PCONS_LUNARS]&") Lunar Fortune")
	GUICtrlSetData($pconsCheckbox[$PCONS_RES], "("&$lPconsQuantity[$PCONS_RES]&") Res Scrolls")
	GUICtrlSetData($pconsCheckbox[$PCONS_SKALESOUP], "("&$lPconsQuantity[$PCONS_SKALESOUP]&") Skalefin Soup")
	GUICtrlSetData($pconsCheckbox[$PCONS_MOBSTOPPERS], "("&$lPconsQuantity[$PCONS_MOBSTOPPERS]&") Mobstoppers")
	GUICtrlSetData($pconsCheckbox[$PCONS_PANHAI], "("&$lPconsQuantity[$PCONS_PANHAI]&") Pahnai Salad")
	GUICtrlSetData($pconsCheckbox[$PCONS_CITY], "("&$lPconsQuantity[$PCONS_CITY]&") City Speedboosts")
EndFunc

Func pconsSetColor($pconID, $quantity, $threshold)
	If $quantity == 0 Then
		GUICtrlSetColor($pconsCheckbox[$pconID], $COLOR_RED)
	ElseIf $quantity > $threshold Then
		GUICtrlSetColor($pconsCheckbox[$pconID], $COLOR_GREEN)
	Else
		GUICtrlSetColor($pconsCheckbox[$pconID], $COLOR_YELLOW)
	EndIf
EndFunc

Func presetLoad()
	Local $lPreset = IniRead($iniFullPath, "pconsPresets", GUICtrlRead(@GUI_CtrlId), 18*"0")
	Local $lArr = StringSplit($lPreset, "")
	If $lArr[0] <> $pconsEffects[0]+1 Then
		WriteChat("Error loading template. Unfortunately presets made before the update will not work.", $GWToolbox)
		Return
	EndIf

	For $i = 0 To $pconsCount-1
		$pconsActive[$i] = $lArr[$i+1] == "1"
	Next

	pconsScanInventory()
EndFunc

Func presetSave()
	Local $lName = ""
	Local $lString = ""
	For $i = 0 To $pconsCount-1
		$lString &= $pconsActive[$i] ? "1" : "0"
	Next

	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $lGui = GUICreate("Save Preset", 160, 150, Default, Default, $WS_POPUP, Default, $mainGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_WHITE)
		WinSetTrans($lGui, "", $Transparency)
	GUICtrlCreateLabel("Save Preset...", 0, 0, 160, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lDefault = "Enter name..."
	If GUICtrlRead($pconsPresetList) <> $pconsPresetDefault Then $lDefault = GUICtrlRead($pconsPresetList)
	Local $lInput = GUICtrlCreateInput($lDefault, 20, 60, 120, 23)
	Local $lCancel = MyGuiCtrlCreateButton("Cancel", 20, 100, 50, 20)
	Local $lOk = MyGuiCtrlCreateButton("Save", 90, 100, 50, 20)

	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[1][2] = [["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $lCancel, $iESC
				ExitLoop
			Case $lOk, $lInput
				$lName = GUICtrlRead($lInput)
				If $lName <> "" And $lName <> $pconsPresetDefault Then IniWrite($iniFullPath, "pconsPresets", $lName, $lString)
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($lGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
	If (($lName <> "") And ($lName <> $pconsPresetDefault) And (_GUICtrlComboBox_FindString($pconsPresetList, $lName)==-1)) Then _GUICtrlComboBox_AddString($pconsPresetList, $lName)
EndFunc

Func presetDelete()
	Local $lName = ""
	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $lGui = GUICreate("Delete Preset", 160, 150, Default, Default, $WS_POPUP, Default, $mainGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_WHITE)
		WinSetTrans($lGui, "", $Transparency)
	GUICtrlCreateLabel("Delete Preset...", 0, 0, 160, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lCombo = GUICtrlCreateCombo("", 20, 60, 120, 23, $CBS_DROPDOWNLIST)
		GUICtrlSetColor(-1, $COLOR_WHITE)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		SetPresetCombo($lCombo)
	Local $lCancel = MyGuiCtrlCreateButton("Cancel", 20, 100, 50, 20)
	Local $lDelete = MyGuiCtrlCreateButton("Delete", 90, 100, 50, 20)

	Local $iENTER = GUICtrlCreateDummy()
	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[2][2] = [["{ENTER}", $iENTER], ["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $lCancel, $iESC
				ExitLoop
			Case $lDelete, $iENTER
				$lName = GUICtrlRead($lCombo)
				If $lName <> $pconsPresetDefault Then
					IniDelete($iniFullPath, "pconsPresets", $lName)
				EndIf
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($lGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
	If (($lName <> "") And ($lName <> $pconsPresetDefault)) Then _GUICtrlComboBox_DeleteString($pconsPresetList, _GUICtrlComboBox_FindString($pconsPresetList, $lName))
	GUICtrlSetData($pconsPresetList, $pconsPresetDefault)
EndFunc

Func SetPresetCombo($aGuiCtrlID, $aDefault = $pconsPresetDefault)
	Local $lPresets = IniReadSection($iniFullPath, "pconsPresets")
	Local $lPresetsString = $aDefault
	If Not @error Then
		For $i=1 To $lPresets[0][0]
			$lPresetsString = $lPresetsString&"|"&$lPresets[$i][0]
		Next
	EndIf
	GUICtrlSetData($aGuiCtrlID, $lPresetsString)
	GUICtrlSetData($aGuiCtrlID, $aDefault)
EndFunc

Func pconsHotkeyToggleActive()
	Local $active = (GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED)
	If @GUI_CtrlId <> $pconsHotkeyActive Then Return MyGuiMsgBox(0, "pconsHotkeyToggleActive", "not implemented!")

	$pconsHotkey = $active
	IniWrite($iniFullPath, $pconsIniSection, "hkActive", $active)
EndFunc
