#include <globals.au3>
#include <myguifuncs.au3>
#include <lib/ColorPicker.au3>
#include <lib/GWA2.au3>


Global $toolboxLabel
Global $sfmacro
Local $sfmacroCheckBoxes[8]
Local $sfmacroSkillsToUse[8]

Func settingsLoadIni()
	$IsOnTop = (IniRead($iniFullPath, "display", "ontop", True) == "True") ; true by default
	$Transparency = IniRead($iniFullPath, "display", "guitransparency", 210)
	$expandLeft = (IniRead($iniFullPath, "display", "expandleft", False) == "True")
	For $i = 0 To 7 Step 1
		$sfmacroSkillsToUse[$i] = (IniRead($iniFullPath, $s_sfmacro, $i + 1, False) == "True")
	Next
EndFunc

Func settingsBuildUI()
	Local $x = 10
	Local $y = 10

	; title and version
	$toolboxLabel = GUICtrlCreateLabel("GWToolbox by Has", $x, $y, 190, 17)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	$y += 20
	GUICtrlCreateLabel("Version " & $currentVersion, $x, $y, 190, 17)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")

	; website and settings folder
	$y += 25
	Global Const $websiteButton = MyGuiCtrlCreateButton("Open GWToolbox Website", $x, $y, 190, 17, 0xAAAAAA, 0x222222, 1)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetTip(-1, "The website where you can find the GWToolbox executable, source code, and detailed features description.")
	$y += 25
	Global Const $settingsButton = MyGuiCtrlCreateButton("Open Settings Folder", $x, $y, 190, 17, 0xAAAAAA, 0x222222, 1)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetTip(-1, "The location where the GWToolbox.ini file is saved with all your settings, you can delete it to reset toolbox or save it to backup and then restore later")
	$y += 25

	; display
	Local $displayX = 10
	Local $displayY = $y
	GUICtrlCreateGroup("Display", $displayX, $displayY, 190, 120)
	Global Const $OnTopCheckbox = GUICtrlCreateCheckbox("Always on top", $displayX + 10, $displayY + 20)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	If $IsOnTop Then GUICtrlSetState($OnTopCheckbox, $GUI_CHECKED)

	Global Const $expandLeftCheckbox = GUICtrlCreateCheckbox("Expand tabs on the left", $displayX + 10, $displayY + 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	If $expandLeft Then GUICtrlSetState($expandLeftCheckbox, $GUI_CHECKED)

	GUICtrlCreateLabel("Transparency:", $displayX + 10, $displayY + 70)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $TransparencySlider = GUICtrlCreateSlider($displayX + 10, $displayY + 90, 170, 23)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetLimit(-1, 255, 40)
	GUICtrlSetData(-1, $Transparency)
	GUICtrlSetBkColor(-1, $COLOR_BLACK)
	$y += 130

	; bonds monitor
	GUICtrlCreateGroup("E/mo Bonds Monitor", $x, $y, 190, 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $infoBuffsDropCheckbox = GUICtrlCreateCheckbox("Drop on click", $x + 10, $y + 15)
	GUICtrlSetOnEvent(-1, "infoBuffsDropToggleActive")
	If $buffsDrop Then GUICtrlSetState(-1, $GUI_CHECKED)
	MyGuiCtrlCreateButton("?", $x + 174, $y + 15, 10, 13)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetFont(-1, 9)
	GUICtrlSetTip(-1, 	"It will show which bonds are used on each party member."&@CRLF& _
						"Place the monitor next to the party window, it will have the same height."&@CRLF& _
						@CRLF& _
						"Notes:"&@CRLF& _
						"- It will work properly only in parties with no heroes or henchmen"&@CRLF& _
						"- It will only show the bonds you are maintaining"&@CRLF& _
						"- If you tick 'Drop on click' you can drop bonds when clicking on the bond in the monitor")
	$y += 55


	GUICtrlCreateGroup("Timer", $x, $y, 190, 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $infoTimerColorPicker = _GUIColorPicker_Create("Color", $x + 20, $y + 15, 100, 25, $COLOR_TIMER, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Timer Color')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_TIMER)
	$y += 55


	GUICtrlCreateGroup("Target Health", $x, $y, 190, 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $infoHealthColorPicker1 = _GUIColorPicker_Create("High", $x + 10, $y + 15, 55, 25, $COLOR_HEALTH_HIGH, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 1')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_HEALTH_HIGH)
	Global Const $infoHealthColorPicker2 = _GUIColorPicker_Create("Middle", $x + 67, $y + 15, 55, 25, $COLOR_HEALTH_MIDDLE, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 2')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_HEALTH_MIDDLE)
	Global Const $infoHealthColorPicker3 = _GUIColorPicker_Create("Low", $x + 124, $y + 15, 55, 25, $COLOR_HEALTH_LOW, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 3')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_HEALTH_LOW)
	$y += 55

	GUICtrlCreateGroup("Target Distance", $x, $y, 190, 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $infoDistanceColorPicker = _GUIColorPicker_Create("Color", $x+20, $y+15, 100, 25, $COLOR_DISTANCE, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Distance Color')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_DISTANCE)
	$y += 55

	; zoom
	Local $zoomX = $x
	Local $zoomY = $y
	GUICtrlCreateGroup("Max Zoom", $zoomX, $zoomY, 190, 50)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	MyGuiCtrlCreateButton("?", $zoomX + 174, $zoomY + 15, 10, 13)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetFont(-1, 9)
	GUICtrlSetTip(-1, "It will increase max camera zoom. Just move the slider and zoom out.")
	Global Const $zoomSlider = GUICtrlCreateSlider($zoomX + 5, $zoomY + 20, 165, 23)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetBkColor(-1, $COLOR_BLACK)
	GUICtrlSetLimit(-1, 20000, 750)
	$y += 60


	GUICtrlCreateGroup("Party Danger", $x, $y, 190, 45)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $infoPartyCheckbox = GUICtrlCreateCheckbox("Show", $x+10, $y+17)
	GUICtrlSetOnEvent(-1, "toggleParty")
	If $party Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoPartyColorPicker = _GUIColorPicker_Create("Color", $x+75, $Y + 15, 80, 25, $COLOR_PARTY, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Party Color')
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetColor(-1, $COLOR_BLACK)
	GUICtrlSetBkColor(-1, $COLOR_PARTY)
	MyGuiCtrlCreateButton("?", $x + 174, $y + 15, 10, 13)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	GUICtrlSetFont(-1, 9)
	GUICtrlSetTip(-1,  "It will show how many enemies are targeting each party member."&@CRLF& _
						"Place the monitor next to the party window, it will have the same height."&@CRLF&@CRLF& _
						"IMPORTANT: This feature uses a lot of CPU power, and may crash the game, expecially"&@CRLF& _
						"when used with a lot of enemies in radar range. Not recommended. Use at your own risk.")
	$y += 55

	; skills clicker
	Local $clickerX = $x
	Local $clickerY = $y
	GUICtrlCreateGroup("Skills Clicker", $clickerX, $clickerY, 190, 105)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	Global Const $sfmacroActive = GUICtrlCreateCheckbox("Active", $clickerX + 10, $clickerY + 20, 60, 17)
	Global Const $sfmacroEmoMode = GUICtrlCreateCheckbox("E/mo", $clickerX + 95, $clickerY + 20, 60, 17)
	MyGuiCtrlCreateButton("?", $clickerX + 174, $clickerY + 15, 10, 13)
	GUICtrlSetFont(-1, 9)
	GUICtrlSetTip(-1, "It will just use selected skills on recharge"&@CRLF& _
	"Note: priority left to right"&@CRLF&@CRLF& _
	"If E/mo is also enabled it will cast ER, spirit bond and burning speed."&@CRLF& _
	"E/mo mode does NOT check for skill numbers below.")
	GUICtrlCreateLabel("Skills to use:", $clickerX + 10, $clickerY + 40, 80, 17)
	GUICtrlSetOnEvent(-1, "settingsEventHandler")
	$sfmacroCheckBoxes[0] = GUICtrlCreateCheckbox("1", $clickerX + 10,  $clickerY + 60, 25, 17)
	$sfmacroCheckBoxes[1] = GUICtrlCreateCheckbox("2", $clickerX + 55,  $clickerY + 60, 25, 17)
	$sfmacroCheckBoxes[2] = GUICtrlCreateCheckbox("3", $clickerX + 100, $clickerY + 60, 25, 17)
	$sfmacroCheckBoxes[3] = GUICtrlCreateCheckbox("4", $clickerX + 145, $clickerY + 60, 25, 17)
	$sfmacroCheckBoxes[4] = GUICtrlCreateCheckbox("5", $clickerX + 10,  $clickerY + 80, 25, 17)
	$sfmacroCheckBoxes[5] = GUICtrlCreateCheckbox("6", $clickerX + 55,  $clickerY + 80, 25, 17)
	$sfmacroCheckBoxes[6] = GUICtrlCreateCheckbox("7", $clickerX + 100, $clickerY + 80, 25, 17)
	$sfmacroCheckBoxes[7] = GUICtrlCreateCheckbox("8", $clickerX + 145, $clickerY + 80, 25, 17)
		GUICtrlSetOnEvent($sfmacroActive, "sfMacroToggleActive")
	If $sfmacro Then GUICtrlSetState($sfmacroActive, $GUI_CHECKED)
	For $i = 0 To 7 Step 1
		If $sfmacroSkillsToUse[$i] == True Then
			GUICtrlSetState($sfmacroCheckBoxes[$i], $GUI_CHECKED)
		EndIf
		GUICtrlSetOnEvent($sfmacroCheckBoxes[$i], "sfmacroToggleSkill")
	Next

	GUICtrlSetState($toolboxLabel, $GUI_FOCUS)
EndFunc

Func sfmacroToggleSkill()
	For $i = 0 To 7 Step 1
		If $sfmacroCheckBoxes[$i] == @GUI_CtrlId Then
			$sfmacroSkillsToUse[$i] = Not $sfmacroSkillsToUse[$i]
			IniWrite($iniFullPath, $s_sfmacro, $i + 1, $sfmacroSkillsToUse[$i])
			Return
		EndIf
	Next
EndFunc   ;==>sfmacroToggleSkill

Func settingsEventHandler()
	Switch @GUI_CtrlId
		Case $OnTopCheckbox
			$IsOnTop = GUICtrlRead($OnTopCheckbox)==$GUI_CHECKED
			WinSetOnTop($mainGui, "", $IsOnTop)
			IniWrite($iniFullPath, "display", "ontop", $IsOnTop)
		Case $TransparencySlider
			IniWrite($iniFullPath, "display", "transparency", GUICtrlRead($TransparencySlider))
			GUICtrlSetState($toolboxLabel, $GUI_FOCUS)
		Case $expandLeftCheckbox
			$expandLeft = (GUICtrlRead($expandLeftCheckbox) == $GUI_CHECKED)
			IniWrite($iniFullPath, "display", "expandleft", $expandLeft)
			MyGuiMsgBox(0, "Info", "You need to restart GWToolbox for this change to have effect")
		Case $infoTimerColorPicker
			Local $lColor = _GuiColorPicker_GetColor($infoTimerColorPicker)
			IniWrite($iniFullPath, "timer", "color", $lColor)
			If $timer Then GUICtrlSetColor($timerGuiLabel, $lColor)
			$COLOR_TIMER = $lColor
			GUICtrlSetBkColor($infoTimerColorPicker, $lColor)
		Case $infoHealthColorPicker1
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker1)
			IniWrite($iniFullPath, "health", "color1", $lColor)
			$COLOR_HEALTH_HIGH = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker1, $lColor)
		Case $infoHealthColorPicker2
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker2)
			IniWrite($iniFullPath, "health", "color2", $lColor)
			$COLOR_HEALTH_MIDDLE = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker2, $lColor)
		Case $infoHealthColorPicker3
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker3)
			IniWrite($iniFullPath, "health", "color3", $lColor)
			$COLOR_HEALTH_LOW = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker3, $lColor)
		Case $infoDistanceColorPicker
			Local $lColor = _GuiColorPicker_GetColor($infoDistanceColorPicker)
			If $distance Then distanceUpdateColor($lColor)
			IniWrite($iniFullPath, "distance", "color3", $lColor)
			$COLOR_DISTANCE = $lColor
			GUICtrlSetBkColor($infoDistanceColorPicker, $lColor)
		Case $infoPartyColorPicker
			Local $lColor = _GUIColorPicker_GetColor($infoPartyColorPicker)
			IniWrite($iniFullPath, "party", "color", $lColor)
			$COLOR_PARTY = $lColor
			If $party Then
				For $i=1 To 8
					GUICtrlSetColor($partyLabels[$i], $lColor)
				Next
			EndIf
			GUICtrlSetBkColor($infoPartyColorPicker, $lColor)
		Case $zoomSlider
			ChangeMaxZoom(GUICtrlRead($zoomSlider))
			GUICtrlSetState($toolboxLabel, $GUI_FOCUS)
		Case $settingsButton
			ShellExecute($DataFolder)
		Case $websiteButton
			ShellExecute($Host)
		Case Else
			GUICtrlSetState($toolboxLabel, $GUI_FOCUS)
	EndSwitch
EndFunc

Func infoBuffsDropToggleActive()
	Local $active = (GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED)
	If @GUI_CtrlId <> $infoBuffsDropCheckbox Then Return MyGuiMsgBox(0, "infoBuffsDropToggleActive", "not implemented!")

	$buffsDrop = $active
	IniWrite($iniFullPath, "buffsdrop", "active", $active)
EndFunc

Func sfMacroToggleActive()
	Local $active = (GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED)
	If @GUI_CtrlId <> $sfmacroActive Then Return MyGuiMsgBox(0, "sfMacroToggleActive", "not implemented!")

	$sfmacro = $active
	IniWrite($iniFullPath, $s_sfmacro, "active", $active)
EndFunc