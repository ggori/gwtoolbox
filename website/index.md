# Guild Wars Toolbox
A set of tools for Guild Wars speed-clearers

_by Has_

---------------
### Download
[GWToolbox.exe](http://fbgmguild.com/GWToolbox/GWToolbox.exe) (version 3.1)

Older versions:
[GWToolbox v2.15.exe](http://fbgmguild.com/GWToolbox/GWToolbox_2.15.exe)

[Changes History](http://fbgmguild.com/GWToolbox/History.txt)

### Features
* Automatically **maintains** selected pcons
* Ability to set **hotkeys** for typing /stuck and /resign, using or cancelling recall and UA, using res scrolls and powerstones and more!
* Send team builds into chat
* Fast travel to **any** outpost
* Take or accept quests quickly with hotkeys, and be able to do so while NPCs are in combat
* Check price for consets, res scrolls and powerstones, automatically buy materials for any amount of those
* Show instance timer, target health or distance, maintained E/Mo bonds

### Source code
GWToolbox is completely open source and the repository is available [here](https://bitbucket.org/ggori/gwtoolbox) on BitBucket

Feel free to download or fork the repository and play with it. You can message me on BitBucket or make a pull request if you make something cool!

### FAQs

__Will I get banned for using GWToolbox?__
I can't say. GWToolbox is not a bot as it it does not play the game for you, however it does use the same technology that bots use. What can I say, though, is that as far as I know noone has ever been banned for it since it was created 2 years ago.

__It's not showing on top of Guild Wars!__
If you are running windowed, make sure you tick "Always on top". If you are running full-screen, then no, it won't show.

__It's not working, can you help?__

* Run **both** Guild Wars and GWToolbox as administrator.
* Add GWToolbox.exe to your antivirus whitelist
* In Windows 8 (and above?) add GWToolbox.exe to the operating system whitelist
* Make sure the Guild Wars client process is called "gw.exe" (not case sensitive)

__It crashed my game!__
Again, running both Guild Wars and GWToolbox as administrator helps with crashing issues.

In some Eye of the North dungeons, GWToolbox **will** crash the game if you zone between levels with pcons usage active. This is a known issue I was not able to solve, the simple workaround is to disable pcons before zoning and re-enable them after.

### Detailed features list and description
Each button in the main interface will open a new "tab" on the side. The last four checkboxes will each create a popup window that can be moved around.

The two top-right buttons are use to `-`minimize or `x`close. You can use the title bar `GWToolbox` to move the main window.

#### Pcons
Tick the desired consumables to use, then click "Toggle Active" **or** the checkbox in the main window. When status is "Enabled" (and checkbox ticked), GWToolbox will maintain those consumables in explorable areas.

A **conset** will only be used once, and only if the following conditions are met: all party members are alive, in radar range, within the first 60 seconds.

You can alternatively set up a hotkey to toggle activation of pcons.

You can save or load consumable presets.

Note that GWToolbox will scan your inventory for pcons everytime you zone into a new map. If you wish to update the scan, for example after taking consumable from the Xunlai chest, simply click "Update".

#### Hotkeys
All hotkeys have an "Hotkey" button that will allow you to select which key to use, and a "Active" checkbox that will allow you to enable or disable the hotkey.

* **/stuck** types `/stuck` in chat
* **recall** cancels recall if it's on, otherwise casts on current target
* **UA** cancels UA if it's on, otherwise casts it
* **/resign** types `/resign` in chat
* **[/resign;xx]** types `[/resign;xx]` in chat
* **Res Scrolls** uses a res scroll
* **/age** types `/age` in chat
* **Age pm** will send you a pm with the current instance time. Useful for full-screen users
* **Toggle Clicker** toggles on/off a clicker that will spam left mouse click
* **Use Pstones** will use a powerstone
* **Use Rainbow** will use red, blue, green rock candies. It will not use one if it is already active.
* **Target Boo** will target the current *Boo*, the ghost created by Ghost-in-the-Box
* **Pop Ghost** will use a Ghost-in-the-Box
* **Pop Ghastly** will use a Ghastly Summoning Stone
* **Pop Legion** will use a Legionnaire Summoning Crystal

Note: Due do lack of popularity Hide GW, Focus, Loot and Identifier hotkeys are now hidden by default. They will be visible to whoever used them before the 3.0 update.

#### Builds
You can send into Team Chat your saved team builds.

First of all, press Edit in any row, and write or paste your team build, then click on the team build name to send it to chat.


#### Fast Travel
First of all select a district, or leave the default `Current District`. Then you can click on one of the buttons for quickly travelling there. Alternatively, you can select any town in the game from the drop-down list and then click `Travel` to go. Note that while selecting the list you can type the outpost name to choose it quickly.

#### Dialogs
When you use any of those dialogs in this tab, you need to target the appropriate npc, then use the button or hotkey.

You can use the four buttons to take those four quests, otherwise you can set up your own hotkeys for most quests.

With UW Teleport you can teleport your team to any area, even if its reaper is not up yet. Teleport works like normal teleport, meaning party-wide before dhuum spawns, and solo teleport after.

Custom Dialog allows you to use any dialog ID. You have to talk to the NPC and then send the ID.

Some useful dialog IDs:

* Kamadan to Consulate Docks ferry: `0x85` @ Assistant Hahnna
* Consulate Docks to Kaineng: `0x88` @ Mhenlo
* Consulate Docks to Lion's Arch: `0x89` @ Mhenlo
* Open the gate to Lion's Arch: `0x85` @ Lionguard Neiro
* Craft armor in fow: `0x7F` @ Eternal Forgemaster

#### Materials
Price check and buy materials for cons, res scrolls, powerstones. Read the help dialog for more info.

#### Settings
* **Open GWToolbox Website** - Opens this web page
* **Open Settings Folder** - GWToolbox stores all your current settings into the `GWToolbox.ini` file that you can find by pressing this button. When upgrading your pc or reformatting, you can backup and then restore this file to keep your settings.
* **Display** - You can choose to display GWToolbox on top of (windowed) Guild Wars, expand tabs on the left instead of the default right side and you can select the transparency.
* **Max zoom** - Increase the maximum zoom. Mostly for fun but can be useful to check hos-jumps. Note that mobs are shown on screen if they are on radar distance from you, regardless of camera position.
* **Party Danger** will show the number of enemies targeting each party member. *Note that this feature requires toolbox to always scan the whole radar and thus it may use a lot of CPU power*
* **Skills Clicker** - Simple "dumb" clicker. It will use the selected skills on recharge. If the E/Mo mode check-box is ticked then it will use Ether Renewal, Spirit Bond, Burning speed on recharge. E/Mo mode is good for going afk as emo, but not for actual play.


#### Info
* **Timer** shows the current instance timer. In Urgoz it will also show door timing.
* **Health** will show target health both as percentage and absolute value. You can only see the absolute value if you directly dealt damage or healed your target.
* **Distance** will show the distance from the target in percentage of in-game standard distances
* **Bonds** will show which bond is being active on each party member. You will have to align the window with your party. Also note that this feature ignores heroes and henchmen. You can choose to drop bonds when clicked (single click) by selecting the check box in the settings. The only bonds displayed are Balthazar Spirit, Protective Bond and Life Bond.