# Guild Wars Toolbox

## A set of tools to help all Guild Wars players

This is the repository of the old, deprecated, AutoIt-based GWToolbox.

The current project being developed and maintained is GWToolbox++ at https://github.com/HasKha/GWToolboxpp